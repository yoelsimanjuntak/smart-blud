# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_simpelblud
# Generation Time: 2023-08-14 17:58:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2023-05-30 22:23:34','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(2,'2023-05-30 22:23:36','http://localhost/tt-dinkes-jkn/site/setting/unit-add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(3,'2023-05-30 22:23:40','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(4,'2023-05-30 22:25:22','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(5,'2023-05-30 22:26:30','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(6,'2023-05-30 22:27:01','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(7,'2023-05-30 22:27:17','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(8,'2023-05-30 22:27:59','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(9,'2023-05-30 22:32:02','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(10,'2023-05-30 22:32:48','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(11,'2023-05-30 22:32:50','http://localhost/tt-dinkes-jkn/site/setting/unit-edit/11.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(12,'2023-05-30 22:32:52','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(13,'2023-05-30 22:33:18','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(14,'2023-05-30 22:33:33','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(15,'2023-05-30 22:33:58','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(16,'2023-05-30 22:34:00','http://localhost/tt-dinkes-jkn/site/setting/unit-edit/11.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(17,'2023-05-30 22:34:06','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(18,'2023-05-30 22:34:09','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(19,'2023-05-30 22:34:21','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(20,'2023-05-30 22:35:01','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(21,'2023-05-30 22:35:16','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(22,'2023-05-30 22:35:40','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(23,'2023-05-31 08:19:09','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(24,'2023-05-31 08:19:09','http://localhost/tt-dinkes-jkn/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(25,'2023-05-31 08:19:15','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(26,'2023-05-31 08:19:19','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(27,'2023-05-31 08:23:34','http://localhost/tt-dinkes-jkn/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(28,'2023-05-31 08:23:35','http://localhost/tt-dinkes-jkn/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(29,'2023-05-31 08:28:06','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(30,'2023-05-31 08:30:36','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(31,'2023-05-31 08:30:49','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(32,'2023-05-31 08:31:13','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(33,'2023-05-31 08:31:25','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(34,'2023-05-31 08:31:44','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(35,'2023-05-31 08:32:14','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(36,'2023-05-31 08:33:19','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(37,'2023-05-31 08:33:56','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(38,'2023-05-31 08:34:06','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(39,'2023-05-31 08:44:06','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(40,'2023-05-31 08:44:12','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(41,'2023-05-31 08:44:56','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(42,'2023-05-31 09:00:38','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(43,'2023-05-31 09:00:47','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(44,'2023-05-31 09:01:09','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(45,'2023-05-31 09:01:46','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(46,'2023-05-31 09:02:54','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(47,'2023-05-31 09:02:54','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(48,'2023-05-31 09:04:24','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(49,'2023-05-31 09:04:31','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(50,'2023-05-31 09:04:31','http://localhost/tt-dinkes-jkn/site/master/barang-form/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(51,'2023-05-31 09:05:07','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(52,'2023-05-31 09:05:07','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(53,'2023-05-31 09:05:40','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(54,'2023-05-31 09:05:40','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(55,'2023-05-31 09:06:07','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(56,'2023-05-31 09:06:10','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(57,'2023-05-31 09:06:22','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(58,'2023-05-31 09:06:22','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(59,'2023-05-31 09:06:32','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(60,'2023-05-31 09:06:32','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(61,'2023-05-31 09:06:41','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(62,'2023-05-31 09:08:04','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(63,'2023-05-31 09:08:16','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(64,'2023-05-31 09:09:07','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(65,'2023-05-31 09:09:12','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(66,'2023-05-31 09:09:12','http://localhost/tt-dinkes-jkn/site/master/barang-form/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(67,'2023-05-31 09:09:17','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(68,'2023-05-31 09:09:17','http://localhost/tt-dinkes-jkn/site/master/barang-form/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(69,'2023-05-31 09:09:42','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(70,'2023-05-31 09:25:02','http://localhost/tt-dinkes-jkn/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(71,'2023-05-31 09:25:03','http://localhost/tt-dinkes-jkn/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(72,'2023-05-31 09:32:18','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(73,'2023-05-31 09:32:44','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(74,'2023-05-31 09:33:04','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(75,'2023-05-31 09:33:22','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(76,'2023-05-31 09:33:40','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(77,'2023-05-31 09:35:30','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(78,'2023-05-31 09:35:39','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(79,'2023-05-31 09:36:28','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(80,'2023-05-31 09:36:38','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(81,'2023-05-31 09:36:51','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(82,'2023-05-31 09:38:00','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(83,'2023-05-31 09:38:11','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(84,'2023-05-31 09:38:28','http://localhost/tt-dinkes-jkn/site/master/barang-form/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(85,'2023-05-31 09:38:31','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(86,'2023-05-31 09:38:42','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(87,'2023-05-31 09:39:21','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(88,'2023-05-31 09:41:14','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(89,'2023-05-31 09:41:25','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(90,'2023-05-31 09:41:34','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(91,'2023-05-31 09:41:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(92,'2023-05-31 09:42:00','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(93,'2023-05-31 09:42:03','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(94,'2023-05-31 09:42:15','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(95,'2023-05-31 09:42:17','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(96,'2023-05-31 09:42:20','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(97,'2023-05-31 09:45:39','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(98,'2023-05-31 09:46:27','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(99,'2023-05-31 09:51:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(100,'2023-05-31 09:53:07','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(101,'2023-05-31 09:53:47','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(102,'2023-05-31 09:53:57','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(103,'2023-05-31 09:55:23','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(104,'2023-05-31 09:55:43','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(105,'2023-05-31 09:55:52','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(106,'2023-05-31 09:56:47','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(107,'2023-05-31 09:56:48','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(108,'2023-05-31 09:56:49','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(109,'2023-05-31 09:57:14','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(110,'2023-05-31 09:57:45','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(111,'2023-05-31 09:58:42','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(112,'2023-05-31 09:59:48','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(113,'2023-05-31 09:59:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(114,'2023-05-31 10:00:08','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(115,'2023-05-31 10:02:02','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(116,'2023-05-31 10:02:09','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(117,'2023-05-31 10:02:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(118,'2023-05-31 10:03:35','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(119,'2023-05-31 10:06:31','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(120,'2023-05-31 10:06:35','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(121,'2023-05-31 10:06:38','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(122,'2023-05-31 10:07:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(123,'2023-05-31 10:08:51','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(124,'2023-05-31 10:11:10','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(125,'2023-05-31 10:11:20','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(126,'2023-05-31 10:11:38','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(127,'2023-05-31 10:11:38','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(128,'2023-05-31 10:11:58','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(129,'2023-05-31 10:15:39','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(130,'2023-05-31 10:15:44','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(131,'2023-05-31 10:17:15','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(132,'2023-05-31 10:17:28','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(133,'2023-05-31 10:19:32','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(134,'2023-05-31 10:19:41','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(135,'2023-05-31 10:19:51','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(136,'2023-05-31 10:19:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(137,'2023-05-31 10:20:26','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(138,'2023-05-31 10:20:26','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(139,'2023-05-31 10:20:33','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(140,'2023-05-31 10:20:34','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(141,'2023-05-31 10:20:47','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(142,'2023-05-31 10:20:52','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(143,'2023-05-31 10:20:55','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(144,'2023-05-31 10:21:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(145,'2023-05-31 10:21:42','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(146,'2023-05-31 10:21:50','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(147,'2023-05-31 10:21:50','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(148,'2023-05-31 10:21:56','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(149,'2023-05-31 10:21:58','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(150,'2023-05-31 10:22:27','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(151,'2023-05-31 10:22:31','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(152,'2023-05-31 10:22:40','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(153,'2023-05-31 10:23:35','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(154,'2023-05-31 10:26:42','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(155,'2023-05-31 10:26:52','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(156,'2023-05-31 10:28:05','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(157,'2023-05-31 10:28:16','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(158,'2023-05-31 10:28:18','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(159,'2023-05-31 10:29:00','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(160,'2023-05-31 10:29:14','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(161,'2023-05-31 10:29:25','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(162,'2023-05-31 10:29:38','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(163,'2023-05-31 10:30:27','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(164,'2023-05-31 10:31:03','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(165,'2023-05-31 10:31:49','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(166,'2023-05-31 10:32:14','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(167,'2023-05-31 10:32:23','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(168,'2023-05-31 10:32:39','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(169,'2023-05-31 10:32:44','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(170,'2023-05-31 10:32:52','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(171,'2023-05-31 10:33:05','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(172,'2023-05-31 10:33:12','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(173,'2023-05-31 10:33:18','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(174,'2023-05-31 10:33:28','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(175,'2023-05-31 10:33:36','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(176,'2023-05-31 10:46:07','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(177,'2023-05-31 10:46:23','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(178,'2023-05-31 10:46:49','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(179,'2023-05-31 10:47:05','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(180,'2023-05-31 10:48:03','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(181,'2023-05-31 10:48:05','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(182,'2023-05-31 10:48:09','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(183,'2023-05-31 10:48:20','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(184,'2023-05-31 10:48:39','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(185,'2023-05-31 10:52:01','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(186,'2023-05-31 10:52:15','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(187,'2023-05-31 10:53:49','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(188,'2023-05-31 10:54:00','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(189,'2023-05-31 10:54:06','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(190,'2023-05-31 10:54:08','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(191,'2023-05-31 10:54:38','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(192,'2023-05-31 10:54:38','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(193,'2023-05-31 10:54:41','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(194,'2023-05-31 10:54:43','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(195,'2023-05-31 10:54:50','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(196,'2023-05-31 10:54:52','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(197,'2023-05-31 10:57:27','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(198,'2023-05-31 10:57:29','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(199,'2023-05-31 10:57:32','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(200,'2023-05-31 10:57:33','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(201,'2023-05-31 10:58:02','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(202,'2023-05-31 10:58:02','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(203,'2023-05-31 10:58:04','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(204,'2023-05-31 10:58:06','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(205,'2023-05-31 10:58:20','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(206,'2023-05-31 10:59:29','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(207,'2023-05-31 10:59:39','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(208,'2023-05-31 11:00:19','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(209,'2023-05-31 11:00:26','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(210,'2023-05-31 11:00:51','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(211,'2023-05-31 11:00:57','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(212,'2023-05-31 11:01:05','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(213,'2023-05-31 11:01:40','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(214,'2023-05-31 11:02:45','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(215,'2023-05-31 11:03:00','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(216,'2023-05-31 11:03:19','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(217,'2023-05-31 11:04:25','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(218,'2023-05-31 11:04:37','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(219,'2023-05-31 11:05:16','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(220,'2023-05-31 11:08:14','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(221,'2023-05-31 11:08:22','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(222,'2023-05-31 11:09:49','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(223,'2023-05-31 11:10:04','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(224,'2023-05-31 11:10:37','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(225,'2023-05-31 11:10:55','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(226,'2023-05-31 11:11:55','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(227,'2023-05-31 11:11:57','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(228,'2023-05-31 11:11:57','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(229,'2023-05-31 11:12:22','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(230,'2023-05-31 11:12:27','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(231,'2023-05-31 11:14:10','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(232,'2023-05-31 11:14:17','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(233,'2023-05-31 11:14:22','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(234,'2023-05-31 11:14:53','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(235,'2023-05-31 11:14:57','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(236,'2023-05-31 11:15:04','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(237,'2023-05-31 11:15:04','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(238,'2023-05-31 11:15:55','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(239,'2023-05-31 11:16:00','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(240,'2023-05-31 11:22:34','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(241,'2023-05-31 11:22:51','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(242,'2023-05-31 11:23:08','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(243,'2023-05-31 11:23:14','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(244,'2023-05-31 11:23:48','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(245,'2023-05-31 11:24:10','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(246,'2023-05-31 11:24:21','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(247,'2023-05-31 11:24:30','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(248,'2023-05-31 11:24:33','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(249,'2023-05-31 11:24:34','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(250,'2023-05-31 11:25:01','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(251,'2023-05-31 11:25:03','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'),
	(252,'2023-07-22 22:18:16','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(253,'2023-07-22 22:27:43','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(254,'2023-07-22 22:27:52','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(255,'2023-07-22 22:31:31','http://localhost/tt-dinkes-jkn/site/data/edit/5.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(256,'2023-07-22 22:33:24','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(257,'2023-07-22 22:37:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(258,'2023-07-22 22:38:44','http://localhost/tt-dinkes-jkn/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(259,'2023-07-22 22:38:45','http://localhost/tt-dinkes-jkn/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(260,'2023-07-22 22:39:24','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(261,'2023-07-22 22:39:34','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(262,'2023-07-22 22:39:53','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(263,'2023-07-22 22:40:02','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(264,'2023-07-22 22:40:56','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(265,'2023-07-22 22:41:07','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(266,'2023-07-22 22:41:20','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(267,'2023-07-22 22:41:32','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(268,'2023-07-22 22:42:58','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(269,'2023-07-22 22:43:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(270,'2023-07-22 22:43:59','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(271,'2023-07-22 22:44:10','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(272,'2023-07-23 15:20:13','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(273,'2023-07-23 15:20:28','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(274,'2023-07-23 15:20:37','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(275,'2023-07-23 15:20:40','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(276,'2023-07-23 15:22:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(277,'2023-07-23 15:23:35','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(278,'2023-07-23 15:24:04','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(279,'2023-07-23 15:24:09','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(280,'2023-07-23 15:24:19','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(281,'2023-07-23 15:24:40','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(282,'2023-07-23 15:24:47','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(283,'2023-07-23 15:29:28','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(284,'2023-07-23 15:29:48','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(285,'2023-07-23 15:30:47','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(286,'2023-07-23 15:30:53','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(287,'2023-07-23 15:31:30','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(288,'2023-07-23 15:33:26','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(289,'2023-07-23 15:35:44','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(290,'2023-07-23 15:35:53','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(291,'2023-07-23 15:42:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(292,'2023-07-23 15:42:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(293,'2023-07-23 15:42:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(294,'2023-07-23 15:44:03','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(295,'2023-07-23 15:44:22','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(296,'2023-07-23 15:45:00','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(297,'2023-07-23 15:45:12','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(298,'2023-07-23 15:48:19','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(299,'2023-07-23 15:48:28','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(300,'2023-07-23 15:52:17','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(301,'2023-07-23 15:52:26','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(302,'2023-07-23 15:53:50','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(303,'2023-07-23 15:53:59','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(304,'2023-07-23 15:54:01','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(305,'2023-07-23 15:54:03','http://localhost/tt-dinkes-jkn/site/setting/unit-add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(306,'2023-07-23 15:54:06','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(307,'2023-07-23 15:54:08','http://localhost/tt-dinkes-jkn/site/master/belanja.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(308,'2023-07-23 15:55:21','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(309,'2023-07-23 15:55:23','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(310,'2023-07-23 15:57:10','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(311,'2023-07-23 15:58:24','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(312,'2023-07-23 15:58:32','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(313,'2023-07-23 15:58:52','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(314,'2023-07-23 15:59:04','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(315,'2023-07-23 16:00:23','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(316,'2023-07-23 16:06:19','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(317,'2023-07-23 19:37:33','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(318,'2023-07-23 19:37:33','http://localhost/tt-dinkes-jkn/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(319,'2023-07-23 19:37:40','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(320,'2023-07-23 19:39:06','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(321,'2023-07-23 19:39:08','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(322,'2023-07-23 19:50:43','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(323,'2023-07-23 19:51:33','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(324,'2023-07-23 19:53:00','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(325,'2023-07-23 19:53:19','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(326,'2023-07-23 20:02:31','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(327,'2023-07-23 20:02:44','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(328,'2023-07-23 20:02:49','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(329,'2023-07-23 20:03:45','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(330,'2023-07-23 20:04:27','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(331,'2023-07-23 20:04:41','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(332,'2023-07-23 20:05:31','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(333,'2023-07-23 20:09:09','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(334,'2023-07-23 20:09:39','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(335,'2023-07-23 20:09:43','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(336,'2023-07-23 20:09:45','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(337,'2023-07-23 20:10:01','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(338,'2023-07-23 20:10:09','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(339,'2023-07-23 20:10:36','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(340,'2023-07-23 20:10:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(341,'2023-07-23 20:11:56','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(342,'2023-07-23 20:15:39','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(343,'2023-07-23 20:17:55','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(344,'2023-07-23 20:18:24','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(345,'2023-07-23 20:19:31','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(346,'2023-07-23 20:20:51','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(347,'2023-07-23 20:21:35','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(348,'2023-07-23 20:30:14','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(349,'2023-07-23 20:31:55','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(350,'2023-07-23 20:32:00','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(351,'2023-07-23 20:32:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(352,'2023-07-23 20:34:00','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(353,'2023-07-23 20:34:06','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(354,'2023-07-23 20:34:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(355,'2023-07-23 20:49:34','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(356,'2023-07-23 20:51:05','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(357,'2023-07-23 20:53:39','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(358,'2023-07-23 20:59:16','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(359,'2023-07-23 21:02:59','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(360,'2023-07-23 21:03:10','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(361,'2023-07-23 21:03:21','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(362,'2023-07-23 21:04:08','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(363,'2023-07-23 21:07:57','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(364,'2023-07-23 22:32:39','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(365,'2023-07-24 10:25:53','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(366,'2023-07-24 10:26:00','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(367,'2023-07-24 10:32:08','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(368,'2023-07-24 10:32:54','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(369,'2023-07-24 10:34:43','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(370,'2023-07-24 10:37:55','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(371,'2023-07-24 10:39:46','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(372,'2023-07-24 10:39:46','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(373,'2023-07-24 10:41:26','http://localhost/tt-dinkes-jkn/site/data/edit/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(374,'2023-07-24 10:41:38','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(375,'2023-07-24 10:42:02','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(376,'2023-07-24 10:42:13','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(377,'2023-07-24 10:42:26','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(378,'2023-07-24 10:46:30','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(379,'2023-07-24 10:47:03','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(380,'2023-07-24 10:47:30','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(381,'2023-07-24 10:47:49','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(382,'2023-07-24 10:50:12','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(383,'2023-07-24 10:50:46','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(384,'2023-07-24 10:50:49','http://localhost/tt-dinkes-jkn/site/data/edit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(385,'2023-07-24 10:51:06','http://localhost/tt-dinkes-jkn/site/data/edit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(386,'2023-07-24 10:51:08','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(387,'2023-07-24 10:51:10','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(388,'2023-07-24 10:53:00','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(389,'2023-07-24 10:53:03','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(390,'2023-07-24 11:02:15','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(391,'2023-07-24 11:02:22','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(392,'2023-07-24 11:02:27','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(393,'2023-07-24 11:02:32','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(394,'2023-07-24 11:03:13','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(395,'2023-07-24 11:06:41','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(396,'2023-07-24 11:06:46','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(397,'2023-07-24 11:06:49','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(398,'2023-07-24 11:07:05','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(399,'2023-07-24 11:07:13','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(400,'2023-07-24 11:20:12','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(401,'2023-07-24 11:20:26','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(402,'2023-07-24 11:20:34','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(403,'2023-07-24 11:20:54','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(404,'2023-07-24 11:28:29','http://localhost/tt-dinkes-jkn/site/data/edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(405,'2023-07-24 11:28:29','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(406,'2023-07-24 11:28:33','http://localhost/tt-dinkes-jkn/site/data/edit/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(407,'2023-07-24 11:28:48','http://localhost/tt-dinkes-jkn/site/data/edit/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(408,'2023-07-24 11:29:34','http://localhost/tt-dinkes-jkn/site/data/edit/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(409,'2023-07-24 11:29:48','http://localhost/tt-dinkes-jkn/site/data/edit/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(410,'2023-07-24 11:29:48','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(411,'2023-07-24 19:10:14','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(412,'2023-07-24 19:10:14','http://localhost/tt-dinkes-jkn/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(413,'2023-07-24 19:18:26','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(414,'2023-07-24 19:20:22','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(415,'2023-07-24 19:20:25','http://localhost/tt-dinkes-jkn/site/data/edit/6.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(416,'2023-07-24 19:21:42','http://localhost/tt-dinkes-jkn/site/data/edit/6.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(417,'2023-07-24 19:21:42','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(418,'2023-07-24 19:21:49','http://localhost/tt-dinkes-jkn/site/data/edit/8.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(419,'2023-07-24 19:22:27','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(420,'2023-07-24 19:22:52','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(421,'2023-07-24 19:23:09','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(422,'2023-07-24 19:23:15','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(423,'2023-07-24 19:23:32','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(424,'2023-07-24 19:23:34','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(425,'2023-07-24 19:24:09','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(426,'2023-07-24 19:24:36','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(427,'2023-07-24 19:24:47','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(428,'2023-07-24 19:24:52','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(429,'2023-07-24 19:24:59','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(430,'2023-07-24 19:25:01','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(431,'2023-07-24 19:25:04','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(432,'2023-07-24 19:25:16','http://localhost/tt-dinkes-jkn/site/master/barang.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(433,'2023-07-24 19:25:30','http://localhost/tt-dinkes-jkn/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(434,'2023-07-24 19:27:38','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(435,'2023-07-24 19:27:41','http://localhost/tt-dinkes-jkn/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(436,'2023-07-24 19:27:45','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(437,'2023-07-24 19:27:45','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(438,'2023-07-24 19:27:50','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(439,'2023-07-24 20:04:33','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(440,'2023-07-24 20:04:40','http://localhost/tt-dinkes-jkn/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(441,'2023-07-24 20:04:40','http://localhost/tt-dinkes-jkn/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(442,'2023-07-24 20:04:53','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(443,'2023-07-24 20:04:56','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(444,'2023-07-24 20:05:03','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(445,'2023-07-24 20:05:14','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(446,'2023-07-24 20:05:17','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(447,'2023-07-24 20:05:24','http://localhost/tt-dinkes-jkn/site/data/edit/8.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(448,'2023-07-24 20:09:49','http://localhost/tt-dinkes-jkn/site/data/edit/8.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(449,'2023-07-24 20:09:58','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(450,'2023-07-24 20:10:00','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(451,'2023-07-24 20:10:09','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(452,'2023-07-24 20:10:10','http://localhost/tt-dinkes-jkn/site/data/setting.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(453,'2023-07-24 20:10:11','http://localhost/tt-dinkes-jkn/site/data/edit/8.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(454,'2023-07-24 20:10:11','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(455,'2023-07-24 20:10:48','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(456,'2023-07-24 20:11:14','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(457,'2023-07-24 20:11:26','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(458,'2023-07-24 20:11:27','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(459,'2023-07-24 20:12:34','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(460,'2023-07-24 20:12:38','http://localhost/tt-dinkes-jkn/site/data/rekapitulasi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(461,'2023-07-24 20:13:41','http://localhost/tt-dinkes-jkn/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(462,'2023-07-24 20:16:51','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(463,'2023-07-24 20:16:53','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(464,'2023-07-24 20:18:28','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(465,'2023-07-24 20:18:28','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(466,'2023-07-24 20:18:30','http://localhost/tt-dinkes-jkn/site/data/edit/13.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(467,'2023-07-24 20:18:48','http://localhost/tt-dinkes-jkn/site/data/edit/13.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(468,'2023-07-24 20:18:48','http://localhost/tt-dinkes-jkn/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(469,'2023-07-24 20:18:59','http://localhost/tt-dinkes-jkn/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
	(470,'2023-08-14 10:27:37','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(471,'2023-08-14 10:28:32','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(472,'2023-08-14 10:28:43','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(473,'2023-08-14 10:28:44','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(474,'2023-08-14 10:28:46','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(475,'2023-08-14 10:28:59','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(476,'2023-08-14 10:29:31','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(477,'2023-08-14 10:29:32','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(478,'2023-08-14 10:30:06','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(479,'2023-08-14 10:30:08','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(480,'2023-08-14 10:31:17','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(481,'2023-08-14 10:31:54','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(482,'2023-08-14 10:33:29','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(483,'2023-08-14 10:37:23','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(484,'2023-08-14 10:39:04','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(485,'2023-08-14 10:39:25','http://localhost/tt-simpelblud/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(486,'2023-08-14 10:39:47','http://localhost/tt-simpelblud/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(487,'2023-08-14 11:43:03','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(488,'2023-08-14 11:43:57','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(489,'2023-08-14 11:44:20','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(490,'2023-08-14 11:44:30','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(491,'2023-08-14 11:44:39','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(492,'2023-08-14 11:45:06','http://localhost/tt-simpelblud/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(493,'2023-08-14 12:36:33','http://localhost/tt-simpelblud/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(494,'2023-08-14 12:36:35','http://localhost/tt-simpelblud/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(495,'2023-08-14 12:36:40','http://localhost/tt-simpelblud/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(496,'2023-08-14 16:35:35','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(497,'2023-08-14 16:35:35','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(498,'2023-08-14 16:36:39','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(499,'2023-08-14 16:36:42','http://localhost/tt-simpelblud/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(500,'2023-08-14 16:36:42','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(501,'2023-08-14 16:36:53','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(502,'2023-08-14 16:37:39','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(503,'2023-08-14 16:37:42','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(504,'2023-08-14 16:38:01','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(505,'2023-08-14 16:38:03','http://localhost/tt-simpelblud/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(506,'2023-08-14 16:38:05','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(507,'2023-08-14 16:38:30','http://localhost/tt-simpelblud/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(508,'2023-08-14 16:38:32','http://localhost/tt-simpelblud/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(509,'2023-08-14 16:39:54','http://localhost/tt-simpelblud/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(510,'2023-08-14 16:39:55','http://localhost/tt-simpelblud/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(511,'2023-08-14 16:45:42','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(512,'2023-08-14 16:46:31','http://localhost/tt-simpelblud/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(513,'2023-08-14 16:46:42','http://localhost/tt-simpelblud/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(514,'2023-08-14 16:46:44','http://localhost/tt-simpelblud/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(515,'2023-08-14 16:46:59','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(516,'2023-08-14 16:47:07','http://localhost/tt-simpelblud/site/data/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(517,'2023-08-14 16:47:09','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(518,'2023-08-14 16:50:01','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(519,'2023-08-14 16:50:02','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(520,'2023-08-14 16:50:04','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(521,'2023-08-14 16:50:10','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(522,'2023-08-14 16:51:46','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(523,'2023-08-14 16:53:46','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(524,'2023-08-14 16:53:54','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(525,'2023-08-14 16:54:15','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(526,'2023-08-14 16:56:10','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(527,'2023-08-14 16:56:21','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(528,'2023-08-14 16:56:33','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(529,'2023-08-14 16:57:18','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(530,'2023-08-14 17:00:04','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(531,'2023-08-14 17:00:59','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(532,'2023-08-14 17:01:05','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(533,'2023-08-14 17:02:07','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(534,'2023-08-14 17:02:11','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(535,'2023-08-14 17:03:05','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(536,'2023-08-14 17:04:01','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(537,'2023-08-14 17:04:14','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(538,'2023-08-14 17:04:37','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(539,'2023-08-14 17:05:10','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(540,'2023-08-14 19:21:14','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(541,'2023-08-14 19:21:35','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(542,'2023-08-14 19:21:55','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(543,'2023-08-14 19:22:14','http://localhost/tt-simpelblud/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(544,'2023-08-14 19:22:14','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(545,'2023-08-14 19:22:43','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(546,'2023-08-14 19:22:52','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(547,'2023-08-14 19:22:56','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(548,'2023-08-14 19:23:06','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(549,'2023-08-14 19:23:43','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(550,'2023-08-14 19:23:49','http://localhost/tt-simpelblud/site/dba/edit/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(551,'2023-08-14 19:24:45','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(552,'2023-08-14 19:26:46','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(553,'2023-08-14 19:27:38','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(554,'2023-08-14 19:28:40','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(555,'2023-08-14 19:30:06','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(556,'2023-08-14 19:31:15','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(557,'2023-08-14 19:32:20','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(558,'2023-08-14 19:33:34','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(559,'2023-08-14 19:33:59','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(560,'2023-08-14 19:34:42','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(561,'2023-08-14 19:35:24','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(562,'2023-08-14 19:35:46','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(563,'2023-08-14 19:35:49','http://localhost/tt-simpelblud/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(564,'2023-08-14 19:35:49','http://localhost/tt-simpelblud/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(565,'2023-08-14 19:35:54','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(566,'2023-08-14 19:36:25','http://localhost/tt-simpelblud/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(567,'2023-08-14 19:36:26','http://localhost/tt-simpelblud/user/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(568,'2023-08-14 19:38:03','http://localhost/tt-simpelblud/user/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(569,'2023-08-14 19:38:07','http://localhost/tt-simpelblud/user/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(570,'2023-08-14 19:38:07','http://localhost/tt-simpelblud/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(571,'2023-08-14 19:38:14','http://localhost/tt-simpelblud/user/edit/admin.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(572,'2023-08-14 19:38:27','http://localhost/tt-simpelblud/user/edit/admin.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(573,'2023-08-14 19:38:35','http://localhost/tt-simpelblud/user/edit/admin.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(574,'2023-08-14 19:41:11','http://localhost/tt-simpelblud/user/edit/admin.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(575,'2023-08-14 19:41:18','http://localhost/tt-simpelblud/user/edit/admin.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(576,'2023-08-14 19:41:18','http://localhost/tt-simpelblud/site/user/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(577,'2023-08-14 19:41:38','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(578,'2023-08-14 19:41:40','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(579,'2023-08-14 19:44:53','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(580,'2023-08-14 19:45:06','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(581,'2023-08-14 19:45:08','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(582,'2023-08-14 19:45:24','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(583,'2023-08-14 19:45:54','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(584,'2023-08-14 19:46:12','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(585,'2023-08-14 19:47:48','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(586,'2023-08-14 19:48:06','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(587,'2023-08-14 19:56:26','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(588,'2023-08-14 19:57:28','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(589,'2023-08-14 19:58:19','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(590,'2023-08-14 19:58:27','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(591,'2023-08-14 19:58:35','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(592,'2023-08-14 19:58:54','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(593,'2023-08-14 19:59:33','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(594,'2023-08-14 19:59:39','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(595,'2023-08-14 20:01:43','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(596,'2023-08-14 20:01:51','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(597,'2023-08-14 20:02:07','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(598,'2023-08-14 20:02:21','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(599,'2023-08-14 20:02:31','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(600,'2023-08-14 20:02:38','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(601,'2023-08-14 20:04:38','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(602,'2023-08-14 20:07:31','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(603,'2023-08-14 20:07:47','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(604,'2023-08-14 20:08:37','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(605,'2023-08-14 20:08:43','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(606,'2023-08-14 20:08:49','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(607,'2023-08-14 20:08:54','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(608,'2023-08-14 20:08:57','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(609,'2023-08-14 20:09:01','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(610,'2023-08-14 20:09:04','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(611,'2023-08-14 20:10:54','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(612,'2023-08-14 20:10:59','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(613,'2023-08-14 20:11:14','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(614,'2023-08-14 20:11:27','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(615,'2023-08-14 20:11:38','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(616,'2023-08-14 20:12:50','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(617,'2023-08-14 22:40:24','http://localhost/tt-simpelblud/site/dba/detail-add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(618,'2023-08-14 22:40:25','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(619,'2023-08-14 22:40:26','http://localhost/tt-simpelblud/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(620,'2023-08-14 22:40:35','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(621,'2023-08-14 22:40:59','http://localhost/tt-simpelblud/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(622,'2023-08-14 22:40:59','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(623,'2023-08-14 22:41:00','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(624,'2023-08-14 22:41:01','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(625,'2023-08-14 22:41:03','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(626,'2023-08-14 22:41:08','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(627,'2023-08-14 22:41:23','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(628,'2023-08-14 22:41:39','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(629,'2023-08-14 22:47:47','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(630,'2023-08-14 22:47:54','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(631,'2023-08-14 22:49:26','http://localhost/tt-simpelblud/site/dba/detail-add/1/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(632,'2023-08-14 22:49:26','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(633,'2023-08-14 22:51:06','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(634,'2023-08-14 22:51:23','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(635,'2023-08-14 22:51:41','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(636,'2023-08-14 22:52:39','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(637,'2023-08-14 22:52:49','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(638,'2023-08-14 22:53:26','http://localhost/tt-simpelblud/site/dba/detail-add/1/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(639,'2023-08-14 22:53:26','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(640,'2023-08-14 22:53:37','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(641,'2023-08-14 22:54:51','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(642,'2023-08-14 22:56:53','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(643,'2023-08-14 22:57:12','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(644,'2023-08-14 22:57:56','http://localhost/tt-simpelblud/site/dba/detail-add/2/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(645,'2023-08-14 22:57:56','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(646,'2023-08-14 22:59:05','http://localhost/tt-simpelblud/site/dba/detail-add/2/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(647,'2023-08-14 22:59:05','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(648,'2023-08-14 23:00:02','http://localhost/tt-simpelblud/site/dba/detail-add/4/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(649,'2023-08-14 23:00:02','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(650,'2023-08-14 23:00:41','http://localhost/tt-simpelblud/site/dba/detail-add/4/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(651,'2023-08-14 23:00:41','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(652,'2023-08-14 23:02:32','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(653,'2023-08-14 23:03:28','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(654,'2023-08-14 23:03:52','http://localhost/tt-simpelblud/site/dba/detail-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(655,'2023-08-14 23:03:52','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(656,'2023-08-14 23:03:59','http://localhost/tt-simpelblud/site/dba/detail-edit/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(657,'2023-08-14 23:03:59','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(658,'2023-08-14 23:14:03','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(659,'2023-08-14 23:14:12','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(660,'2023-08-14 23:14:25','http://localhost/tt-simpelblud/site/dba/detail-add/4/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(661,'2023-08-14 23:14:25','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(662,'2023-08-14 23:14:51','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(663,'2023-08-14 23:16:19','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(664,'2023-08-14 23:16:22','http://localhost/tt-simpelblud/site/data/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(665,'2023-08-14 23:17:01','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(666,'2023-08-14 23:17:12','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(667,'2023-08-14 23:18:15','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(668,'2023-08-14 23:20:27','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(669,'2023-08-14 23:20:44','http://localhost/tt-simpelblud/site/dba/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(670,'2023-08-14 23:21:12','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(671,'2023-08-14 23:21:22','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(672,'2023-08-14 23:21:26','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(673,'2023-08-14 23:23:15','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(674,'2023-08-14 23:23:48','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(675,'2023-08-14 23:24:01','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(676,'2023-08-14 23:24:21','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(677,'2023-08-14 23:24:28','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(678,'2023-08-14 23:24:43','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(679,'2023-08-14 23:27:32','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(680,'2023-08-14 23:27:38','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(681,'2023-08-14 23:28:31','http://localhost/tt-simpelblud/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(682,'2023-08-14 23:28:56','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(683,'2023-08-14 23:31:25','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(684,'2023-08-14 23:31:34','http://localhost/tt-simpelblud/site/dba/detail/4.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(685,'2023-08-14 23:31:41','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(686,'2023-08-14 23:32:03','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(687,'2023-08-14 23:32:08','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(688,'2023-08-14 23:33:43','http://localhost/tt-simpelblud/site/dba/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(689,'2023-08-14 23:33:53','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(690,'2023-08-14 23:34:03','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(691,'2023-08-14 23:43:57','http://localhost/tt-simpelblud/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(692,'2023-08-14 23:43:58','http://localhost/tt-simpelblud/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(693,'2023-08-14 23:52:25','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(694,'2023-08-14 23:52:32','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(695,'2023-08-14 23:52:56','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(696,'2023-08-14 23:53:09','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(697,'2023-08-14 23:53:20','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(698,'2023-08-14 23:53:29','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(699,'2023-08-14 23:53:45','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(700,'2023-08-15 00:03:13','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(701,'2023-08-15 00:03:22','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(702,'2023-08-15 00:03:53','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(703,'2023-08-15 00:04:21','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(704,'2023-08-15 00:05:02','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(705,'2023-08-15 00:07:05','http://localhost/tt-simpelblud/site/invoice/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(706,'2023-08-15 00:07:36','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(707,'2023-08-15 00:07:56','http://localhost/tt-simpelblud/site/invoice/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(708,'2023-08-15 00:09:59','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(709,'2023-08-15 00:10:19','http://localhost/tt-simpelblud/site/invoice/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(710,'2023-08-15 00:11:05','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(711,'2023-08-15 00:11:36','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(712,'2023-08-15 00:12:41','http://localhost/tt-simpelblud/site/invoice/index.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(713,'2023-08-15 00:13:03','http://localhost/tt-simpelblud/site/invoice/add.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(714,'2023-08-15 00:13:08','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(715,'2023-08-15 00:14:25','http://localhost/tt-simpelblud/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(716,'2023-08-15 00:26:08','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(717,'2023-08-15 00:26:27','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(718,'2023-08-15 00:26:39','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(719,'2023-08-15 00:28:26','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(720,'2023-08-15 00:28:35','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(721,'2023-08-15 00:29:08','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(722,'2023-08-15 00:29:24','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(723,'2023-08-15 00:29:31','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(724,'2023-08-15 00:29:44','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(725,'2023-08-15 00:31:07','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(726,'2023-08-15 00:31:23','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(727,'2023-08-15 00:31:49','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(728,'2023-08-15 00:31:54','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(729,'2023-08-15 00:32:05','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(730,'2023-08-15 00:32:13','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(731,'2023-08-15 00:32:30','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(732,'2023-08-15 00:33:24','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(733,'2023-08-15 00:33:32','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(734,'2023-08-15 00:34:00','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(735,'2023-08-15 00:34:09','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(736,'2023-08-15 00:34:41','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(737,'2023-08-15 00:35:29','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(738,'2023-08-15 00:35:39','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(739,'2023-08-15 00:37:04','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(740,'2023-08-15 00:37:13','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(741,'2023-08-15 00:37:30','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(742,'2023-08-15 00:37:38','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(743,'2023-08-15 00:37:42','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(744,'2023-08-15 00:37:53','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(745,'2023-08-15 00:37:59','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(746,'2023-08-15 00:38:03','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(747,'2023-08-15 00:38:24','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(748,'2023-08-15 00:39:59','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(749,'2023-08-15 00:40:10','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(750,'2023-08-15 00:40:22','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(751,'2023-08-15 00:40:30','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(752,'2023-08-15 00:40:35','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(753,'2023-08-15 00:40:53','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(754,'2023-08-15 00:41:03','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(755,'2023-08-15 00:43:28','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(756,'2023-08-15 00:43:48','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(757,'2023-08-15 00:44:02','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(758,'2023-08-15 00:44:21','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(759,'2023-08-15 00:44:49','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(760,'2023-08-15 00:45:18','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(761,'2023-08-15 00:45:41','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(762,'2023-08-15 00:46:30','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(763,'2023-08-15 00:47:10','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(764,'2023-08-15 00:47:53','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(765,'2023-08-15 00:49:39','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(766,'2023-08-15 00:50:02','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(767,'2023-08-15 00:53:51','http://localhost/tt-simpelblud/site/invoice/detail/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Verifikator'),
	(3,'PPTK'),
	(4,'Bendahara');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SIMPEL BLUD'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Manajemen Pembayaran Belanja BLUD'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','RSUD dr. Kumpulan Pane Tebing Tinggi'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Kota Tebing Tinggi, Sumatera Utara'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI'),
	(17,17,'SETTING_PERIOD_FROM','SETTING_PERIOD_FROM','2023-05-01'),
	(18,18,'SETTING_PERIOD_TO','SETTING_PERIOD_TO','2023-08-31');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	(1,'admin','administrator@rsudkumpulanpane.tebingtinggikota.go.id','12','Partopi Tao',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	(16,'fenty','fenty@rsudkumpulanpane.tebingtinggikota.go.id',NULL,'FENTY',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-08-14');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2023-05-30 21:55:35','::1'),
	(16,'fenty','e10adc3949ba59abbe56e057f20f883e',1,0,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mbelanja1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mbelanja1`;

CREATE TABLE `mbelanja1` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `BelNama` varchar(50) NOT NULL DEFAULT '',
  `BelKeterangan` text,
  `BelRekening` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mbelanja1` WRITE;
/*!40000 ALTER TABLE `mbelanja1` DISABLE KEYS */;

INSERT INTO `mbelanja1` (`Uniq`, `BelNama`, `BelKeterangan`, `BelRekening`)
VALUES
	(1,'Belanja Operasi',NULL,'5.1'),
	(2,'Belanja Modal',NULL,'5.2');

/*!40000 ALTER TABLE `mbelanja1` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mbelanja2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mbelanja2`;

CREATE TABLE `mbelanja2` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdBelanja1` bigint(10) unsigned NOT NULL,
  `BelNama` varchar(50) NOT NULL DEFAULT '',
  `BelKeterangan` text,
  `BelRekening` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mbelanja2` WRITE;
/*!40000 ALTER TABLE `mbelanja2` DISABLE KEYS */;

INSERT INTO `mbelanja2` (`Uniq`, `IdBelanja1`, `BelNama`, `BelKeterangan`, `BelRekening`)
VALUES
	(1,1,'Belanja Pegawai',NULL,'5.1.01'),
	(2,1,'Belanja Barang dan Jasa',NULL,'5.1.02'),
	(3,2,'Belanja Tanah',NULL,'5.2.01'),
	(4,2,'Belanja Peralatan dan Mesin',NULL,'5.2.02'),
	(5,2,'Belanja Gedung dan Bangunan',NULL,'5.2.03'),
	(6,2,'Belanja Jalan, Irigasi dan Jaringan',NULL,'5.2.04'),
	(7,2,'Belanja Aset Tetap Lainnya',NULL,'5.2.05'),
	(8,2,'Belanja Aset Lainnya',NULL,'5.2.06');

/*!40000 ALTER TABLE `mbelanja2` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table munit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `munit`;

CREATE TABLE `munit` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UnitNama` varchar(100) DEFAULT NULL,
  `UnitTipe` varchar(50) DEFAULT NULL,
  `UnitAlamat` text,
  `UnitPimpinan` varchar(100) DEFAULT NULL,
  `UnitPimpinanNIP` varchar(100) DEFAULT NULL,
  `UnitPimpinanPangkat` varchar(100) DEFAULT NULL,
  `UnitGoogleLink` text,
  `UnitTelp` varchar(100) DEFAULT NULL,
  `UnitEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `munit` WRITE;
/*!40000 ALTER TABLE `munit` DISABLE KEYS */;

INSERT INTO `munit` (`Uniq`, `UnitNama`, `UnitTipe`, `UnitAlamat`, `UnitPimpinan`, `UnitPimpinanNIP`, `UnitPimpinanPangkat`, `UnitGoogleLink`, `UnitTelp`, `UnitEmail`)
VALUES
	(12,'Bagian Keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,'Bagian Perencanaan dan Evaluasi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,'Bagian Tata Usaha',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,'Bidang Pelayanan Publik',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,'Bidang Pelayanan Keperawatan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,'Bidang Sarpras dan Penunjang Pelayanan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `munit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdba
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdba`;

CREATE TABLE `tdba` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DBATahun` int(11) NOT NULL,
  `DBAJudul` varchar(50) DEFAULT NULL,
  `DBAKeterangan` text,
  `DBAIsAktif` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tdba` WRITE;
/*!40000 ALTER TABLE `tdba` DISABLE KEYS */;

INSERT INTO `tdba` (`Uniq`, `DBATahun`, `DBAJudul`, `DBAKeterangan`, `DBAIsAktif`, `CreatedOn`, `CreatedBy`, `UpdatedOn`, `UpdatedBy`)
VALUES
	(4,2023,'DBA INDUK','Testing',1,'2023-08-14 19:23:06','admin',NULL,''),
	(5,2023,'DBA PERGESERAN','Testing',0,'2023-08-14 23:20:44','admin',NULL,'');

/*!40000 ALTER TABLE `tdba` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdba_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdba_det`;

CREATE TABLE `tdba_det` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdDba` bigint(10) unsigned NOT NULL,
  `IdBelanja` bigint(10) unsigned NOT NULL,
  `DBAKeterangan` varchar(200) NOT NULL DEFAULT '',
  `DBARekening` varchar(50) NOT NULL DEFAULT '',
  `Pagu` double NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tdba_det` WRITE;
/*!40000 ALTER TABLE `tdba_det` DISABLE KEYS */;

INSERT INTO `tdba_det` (`Uniq`, `IdDba`, `IdBelanja`, `DBAKeterangan`, `DBARekening`, `Pagu`, `CreatedOn`, `CreatedBy`, `UpdatedOn`, `UpdatedBy`)
VALUES
	(1,4,1,'Gaji Pegawai Non PNS','5.1.01.01',3155020662,'2023-08-14 22:49:26','admin','2023-08-14 23:03:59','admin'),
	(2,4,1,'Insentif Non PNS','5.1.01.02',587100000,'2023-08-14 22:53:26','admin',NULL,NULL),
	(3,4,2,'Belanja Perjalanan Dinas','5.1.02.04',560000000,'2023-08-14 22:57:56','admin',NULL,NULL),
	(4,4,2,'Belanja Uang dan / atau Jasa untuk diberikan kepada Pihak Ketiga / Pihak Lain / Masyarakat','5.1.02.05',365000000,'2023-08-14 22:59:05','admin',NULL,NULL),
	(6,4,4,'Belanja Modal Peralatan dan Mesin - Printer','5.2.02.04',23997000,'2023-08-14 23:00:41','admin',NULL,NULL),
	(7,4,4,'Belanja Modal Peralatan dan Mesin - Pengadaan Peralatan / Jaringan dan Instalasi Internet (SIMRS)','5.2.02.01',2100000,'2023-08-14 23:14:25','admin',NULL,NULL);

/*!40000 ALTER TABLE `tdba_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembayaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembayaran`;

CREATE TABLE `tpembayaran` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdDba` bigint(10) unsigned NOT NULL,
  `IdUnit` bigint(10) unsigned NOT NULL,
  `PembNomor` varchar(50) NOT NULL DEFAULT '',
  `PembKeterangan` text,
  `PembNominal` double NOT NULL,
  `PembStatus` enum('BARU','VERIFIKASI','PEMBAYARAN','SELESAI') NOT NULL DEFAULT 'BARU',
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembayaran` WRITE;
/*!40000 ALTER TABLE `tpembayaran` DISABLE KEYS */;

INSERT INTO `tpembayaran` (`Uniq`, `IdDba`, `IdUnit`, `PembNomor`, `PembKeterangan`, `PembNominal`, `PembStatus`, `CreatedOn`, `CreatedBy`, `UpdatedOn`, `UpdatedBy`)
VALUES
	(2,6,15,'001/081501/RSKP/2023','EPSON L5190',1050000,'BARU','2023-08-15 00:13:03','admin',NULL,NULL);

/*!40000 ALTER TABLE `tpembayaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembayaran_doc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembayaran_doc`;

CREATE TABLE `tpembayaran_doc` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdPembayaran` bigint(10) unsigned NOT NULL,
  `DocJudul` varchar(200) DEFAULT NULL,
  `DocFile` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tpembayaran_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembayaran_log`;

CREATE TABLE `tpembayaran_log` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdPembayaran` bigint(10) unsigned NOT NULL,
  `LogStatus` enum('BARU','VERIFIKASI','PEMBAYARAN','SELESAI') NOT NULL DEFAULT 'BARU',
  `LogKeterangan` text,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembayaran_log` WRITE;
/*!40000 ALTER TABLE `tpembayaran_log` DISABLE KEYS */;

INSERT INTO `tpembayaran_log` (`Uniq`, `IdPembayaran`, `LogStatus`, `LogKeterangan`, `CreatedOn`, `CreatedBy`)
VALUES
	(1,0,'BARU','Entri Data','2023-08-15 00:10:19','admin'),
	(2,0,'BARU','Entri Data','2023-08-15 00:13:03','admin');

/*!40000 ALTER TABLE `tpembayaran_log` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
