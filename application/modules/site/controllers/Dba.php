<?php
class Dba extends MY_Controller {
  public function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      redirect('site/user/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Dokumen Anggaran";
    $this->template->load('backend', 'dba/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DBATAHUN=>'desc');
    $orderables = array(null,COL_DBATAHUN,null,null,null,null,null);
    $cols = array(COL_DBATAHUN,COL_DBAJUDUL);

    $queryAll = $this->db->get(TBL_TDBA);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tdba.*, (select sum(Pagu) from tdba_det det where det.IdDba = tdba.Uniq) as Pagu')
    ->get_compiled_select(TBL_TDBA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/dba/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="far fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/dba/activate/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-success btn-action" data-toggle="tooltip" data-placement="top" data-title="Aktivasi"><i class="far fa-refresh"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/dba/detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary" data-toggle="tooltip" data-placement="top" data-title="Lihat Rincian"><i class="far fa-search"></i></a>&nbsp;';

      $_ket = strlen($r[COL_DBAKETERANGAN]) > 50 ? substr($r[COL_DBAKETERANGAN], 0, 50) . "..." : $r[COL_DBAKETERANGAN];
      $data[] = array(
        $htmlBtn,
        $r[COL_DBATAHUN],
        $r[COL_DBAJUDUL],
        $_ket,
        (!empty($r[COL_PAGU])?number_format($r[COL_PAGU]):0),
        $r[COL_DBAISAKTIF]==1?'<span class="badge badge-success">AKTIF</span>':'<span class="badge badge-secondary">INAKTIF</span>',
        date('d-m-Y H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      $dat = array(
        COL_DBATAHUN=>$this->input->post(COL_DBATAHUN),
        COL_DBAJUDUL=>$this->input->post(COL_DBAJUDUL),
        COL_DBAKETERANGAN=>$this->input->post(COL_DBAKETERANGAN),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TDBA, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Dokumen Anggaran berhasil diinput.');
      exit();
    } else {
      $this->load->view('dba/form');
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDBA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TDBA);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Dokumen Anggaran berhasil dihapus.');
  }

  public function activate($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDBA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ." != ", $id)->update(TBL_TDBA, array(COL_DBAISAKTIF=>0));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TDBA, array(COL_DBAISAKTIF=>1));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }

    $this->db->trans_commit();
    ShowJsonSuccess('Dokumen Anggaran berhasil diaktivasi.');
    exit();
  }

  public function detail($id) {
    $ruser = GetLoggedUser();
    $rdba = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDBA)
    ->row_array();

    if(empty($rdba)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'Rincian Anggaran';
    $data['data'] = $rdba;
    $this->template->load('backend', 'dba/detail', $data);
  }

  public function detail_add($idBelanja, $idDba) {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      $dat = array(
        COL_IDBELANJA=>$idBelanja,
        COL_IDDBA=>$idDba,
        COL_DBAKETERANGAN=>$this->input->post(COL_DBAKETERANGAN),
        COL_DBAREKENING=>$this->input->post(COL_DBAREKENING),
        COL_PAGU=>toNum($this->input->post(COL_PAGU)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TDBA_DET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Rincian Anggaran berhasil diinput.');
      exit();
    } else {
      $this->load->view('dba/form-detail');
    }
  }

  public function detail_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDBA_DET)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_DBAKETERANGAN=>$this->input->post(COL_DBAKETERANGAN),
        COL_DBAREKENING=>$this->input->post(COL_DBAREKENING),
        COL_PAGU=>toNum($this->input->post(COL_PAGU)),
        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TDBA_DET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Rincian Anggaran berhasil diperbarui.');
      exit();
    } else {
      $data['data'] = $rdata;
      $this->load->view('dba/form-detail', $data);
    }
  }

  public function detail_delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDBA_DET)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TDBA_DET);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Rincian Anggaran berhasil dihapus.');
  }
}
