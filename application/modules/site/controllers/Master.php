<?php
class Master extends MY_Controller {

  public function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      redirect('site/user/dashboard');
    }
  }

  public function belanja() {
    $data['title'] = "Kategori Belanja";
    $this->template->load('backend', 'master/belanja', $data);
  }

  public function belanja_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_BELNAMA,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_BELNAMA,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->where(COL_ISDELETED,0)
    ->get(TBL_MBELANJA);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_ISDELETED,0)
    ->order_by(TBL_MBELANJA.".".COL_BELNAMA, 'asc')
    ->get_compiled_select(TBL_MBELANJA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/belanja-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/belanja-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_BELNAMA].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_BELNAMA],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function belanja_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_BELNAMA => $this->input->post(COL_BELNAMA),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MBELANJA, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function belanja_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_BELNAMA => $this->input->post(COL_BELNAMA),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MBELANJA, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function belanja_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MBELANJA, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function barang() {
    $data['title'] = "Kategori Barang";
    $this->template->load('backend', 'master/barang', $data);
  }

  public function barang_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_BRGNAMA,COL_BELNAMA,null,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_BRGNAMA,COL_BELNAMA,COL_BRGKETERANGAN,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL_MBELANJA,TBL_MBELANJA.'.'.COL_UNIQ." = ".TBL_MBARANG.".".COL_IDBELANJA,"inner")
    ->where(TBL_MBARANG.'.'.COL_ISDELETED,0)
    ->where(TBL_MBELANJA.'.'.COL_ISDELETED,0)
    ->get(TBL_MBARANG);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDON) $item = 'mbarang.'.COL_CREATEDON;
      if($item == COL_CREATEDBY) $item = 'mbarang.'.COL_CREATEDBY;

      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('mbarang.*, mbelanja.BelNama, mbarang.Uniq as ID')
    ->join(TBL_MBELANJA,TBL_MBELANJA.'.'.COL_UNIQ." = ".TBL_MBARANG.".".COL_IDBELANJA,"inner")
    ->where(TBL_MBARANG.'.'.COL_ISDELETED,0)
    ->where(TBL_MBELANJA.'.'.COL_ISDELETED,0)
    ->order_by(TBL_MBARANG.".".COL_BRGNAMA, 'asc')
    ->get_compiled_select(TBL_MBARANG, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/barang-delete/'.$r['ID']).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/barang-form/edit/'.$r['ID']).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_BRGNAMA].'" data-belanja="'.$r[COL_IDBELANJA].'" data-ket="'.$r[COL_BRGKETERANGAN].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
      $_ket = strlen($r[COL_BRGKETERANGAN]) > 50 ? substr($r[COL_BRGKETERANGAN], 0, 50) . "..." : $r[COL_BRGKETERANGAN];

      $data[] = array(
        $htmlBtn,
        $r[COL_BRGNAMA],
        $r[COL_BELNAMA],
        $_ket,
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function barang_form($mode, $id=null) {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDBELANJA=>$this->input->post(COL_IDBELANJA),
            COL_BRGNAMA=>$this->input->post(COL_BRGNAMA),
            COL_BRGKETERANGAN=>$this->input->post(COL_BRGKETERANGAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_MBARANG, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('Input data baru berhasil.');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }  else if($mode=='edit') {
        if(empty($id)) {
          ShowJsonError('Parameter tidak valid!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDBELANJA=>$this->input->post(COL_IDBELANJA),
            COL_BRGNAMA=>$this->input->post(COL_BRGNAMA),
            COL_BRGKETERANGAN=>$this->input->post(COL_BRGKETERANGAN),
          );

          $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MBARANG, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('Pembaruan data berhasil.');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      $data=array();
      if($mode=='edit') {
        $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_MBARANG)->row_array();
      }
      $this->load->view('site/master/barang-form', $data);
    }
  }

  public function barang_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MBARANG, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Data berhasil dihapus.');
  }
}
