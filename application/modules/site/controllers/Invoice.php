<?php
class Invoice extends MY_Controller {
  public function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function index() {
    $data['title'] = "Dokumen Pembayaran";
    $this->template->load('backend', 'invoice/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_UNITNAMA,null,COL_DBAKETERANGAN,null,null,null);
    $cols = array(COL_UNITNAMA,COL_PEMBNOMOR,COL_DBAKETERANGAN);

    $queryAll = $this->db->get(TBL_TPEMBAYARAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_UNITNAMA) $item = TBL_MUNIT.'.'.COL_UNITNAMA;
      if($item == COL_DBAKETERANGAN) $item = TBL_TDBA_DET.'.'.COL_DBAKETERANGAN;

      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpembayaran.*, munit.UnitNama, tdba_det.DBAKeterangan')
    ->join(TBL_MUNIT,TBL_MUNIT.'.'.COL_UNIQ." = ".TBL_TPEMBAYARAN.".".COL_IDUNIT,"inner")
    ->join(TBL_TDBA_DET,TBL_TDBA_DET.'.'.COL_UNIQ." = ".TBL_TPEMBAYARAN.".".COL_IDDBA,"inner")
    ->get_compiled_select(TBL_TPEMBAYARAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/invoice/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="far fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/invoice/detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary" data-toggle="tooltip" data-placement="top" data-title="Lihat Rincian"><i class="far fa-search"></i></a>&nbsp;';

      $htmlStatus = '<span class="badge badge-secondary">'.$r[COL_PEMBSTATUS].'</span>';
      if($r[COL_PEMBSTATUS]=='VERIFIKASI') $htmlStatus = '<span class="badge badge-primary">'.$r[COL_PEMBSTATUS].'</span>';
      else if($r[COL_PEMBSTATUS]=='PEMBAYARAN') $htmlStatus = '<span class="badge badge-info">'.$r[COL_PEMBSTATUS].'</span>';
      else if($r[COL_PEMBSTATUS]=='SELESAI') $htmlStatus = '<span class="badge badge-success">'.$r[COL_PEMBSTATUS].'</span>';
      $data[] = array(
        $htmlBtn,
        $r[COL_UNITNAMA],
        $r[COL_PEMBNOMOR],
        $r[COL_DBAKETERANGAN],
        (!empty($r[COL_PEMBNOMINAL])?number_format($r[COL_PEMBNOMINAL]):0),
        $htmlStatus,
        date('d-m-Y H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      $dat = array(
        COL_IDDBA=>$this->input->post(COL_IDDBA),
        COL_IDUNIT=>$this->input->post(COL_IDUNIT),
        COL_PEMBNOMOR=>$this->input->post(COL_PEMBNOMOR),
        COL_PEMBKETERANGAN=>$this->input->post(COL_PEMBKETERANGAN),
        COL_PEMBNOMINAL=>toNum($this->input->post(COL_PEMBNOMINAL)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );
      if($ruser[COL_ROLEID]==ROLEPPTK) {
        $dat[COL_IDUNIT]=$ruser[COL_COMPANYID];
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPEMBAYARAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $lastid = $this->db->insert_id();

        $res = $this->db->insert(TBL_TPEMBAYARAN_LOG, array(COL_LOGKETERANGAN=>'Entri Data',COL_CREATEDON=>date('Y-m-d H:i:s'),COL_CREATEDBY=>$ruser[COL_USERNAME]));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Dokumen Pembayaran berhasil diinput.');
      exit();
    } else {
      $this->load->view('invoice/form');
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPEMBAYARAN)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TPEMBAYARAN);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Dokumen Pembayaran berhasil dihapus.');
  }

  public function detail($id) {
    $ruser = GetLoggedUser();
    $rinv = $this->db
    ->select('tpembayaran.*, munit.UnitNama, tdba_det.DBARekening, tdba_det.DBAKeterangan')
    ->join(TBL_MUNIT,TBL_MUNIT.'.'.COL_UNIQ." = ".TBL_TPEMBAYARAN.".".COL_IDUNIT,"inner")
    ->join(TBL_TDBA_DET,TBL_TDBA_DET.'.'.COL_UNIQ." = ".TBL_TPEMBAYARAN.".".COL_IDDBA,"inner")
    ->where(TBL_TPEMBAYARAN.'.'.COL_UNIQ, $id)
    ->get(TBL_TPEMBAYARAN)
    ->row_array();

    if(empty($rinv)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'Rincian Pembayaran';
    $data['data'] = $rinv;
    $this->template->load('backend', 'invoice/detail', $data);
  }
}
