<?php
$user = GetLoggedUser();
?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/data/add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>TANGGAL</th>
                    <th>NO. REFERENSI</th>
                    <th>JLH. RINCIAN</th>
                    <th>TOTAL</th>
                    <th>DIBUAT OLEH</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <?php
  if($user[COL_ROLEID] == ROLEADMIN) {
    ?>
    <div class="form-group row">
      <label class="col-sm-2 mb-0">UNIT</label>
      <div class="col-sm-5">
        <select class="form-control" name="filterIdPuskesmas" style="width: 100%" required readonly>
          <?=GetCombobox("SELECT * FROM munit ORDER BY UnitNama", COL_UNIQ, COL_UNITNAMA)?>
        </select>
      </div>
    </div>
    <?php
  } else {
    $rpuskesmas = $this->db
    ->where(COL_UNIQ, $user[COL_COMPANYID])
    ->get(TBL_MUNIT)
    ->row_array();
    ?>
    <div class="form-group row">
      <label class="col-sm-2 mb-0">UNIT</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" value="<?=!empty($rpuskesmas)?$rpuskesmas[COL_UNITNAMA]:'--'?>" disabled />
        <input type="hidden" name="filterIdPuskesmas" value="<?=$user[COL_COMPANYID]?>" />
      </div>
    </div>

    <?php
  }
  ?>
  <div class="form-group row">
    <label class="col-sm-2 mb-0">PERIODE</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
    </div>
    <label class="col-sm-1 mb-0 text-center">s.d</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
    </div>
  </div>
</div>

<script type="text/javascript">
var modal = $('#modal-form');
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/data/index-load')?>",
      "type": 'POST',
      "data": function(data){
        var dateFrom = $('[name=filterDateFrom]', $('.filtering')).val();
        var dateTo = $('[name=filterDateTo]', $('.filtering')).val();
        var idPuskesmas = $('[name=filterIdPuskesmas]', $('.filtering')).val();

        data.dateFrom = dateFrom;
        data.dateTo = dateTo;
        data.idPuskesmas = idPuskesmas;
        //data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    //"dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [[ 1, "desc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1,3,4], "className":'nowrap dt-body-right'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true,"width": "50px"},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });

  $("div.filtering").html($('#dom-filter').html())/*.addClass('d-inline-block ml-2')*/;

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });
});
</script>
