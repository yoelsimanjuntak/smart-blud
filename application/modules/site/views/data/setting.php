<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/data/index')?>">Realisasi Belanja</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-7">
        <div class="card card-outline card-default">
          <div class="card-header">
            <a href="<?=site_url('site/data/index')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group row">
                  <label class="control-label col-sm-3">MULAI</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker text-right" name="DateFrom" value="<?=GetSetting('SETTING_PERIOD_FROM')?>" placeholder="Mulai" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">SELESAI</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker text-right" name="DateTo" value="<?=GetSetting('SETTING_PERIOD_TO')?>" placeholder="Selesai" required />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="card card-outline card-default">
          <div class="card-header">
            <h5 class="card-title font-weight-bold">PETUNJUK</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <p>Pengaturan tanggal <strong>MULAI</strong> dan <strong>SELESAI</strong> akan membatasi periode pengisian data oleh operator masing-masing unit.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          console.log(res);
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          console.log(data)
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
