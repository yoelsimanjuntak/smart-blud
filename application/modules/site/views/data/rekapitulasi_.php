<?php
/*$rbelanja = $this->db
->where(COL_ISDELETED, 0)
->order_by(COL_BELNAMA,'asc')
->get(TBL_MBELANJA)
->result_array();*/
if(!empty($cetak)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Rekapitulasi Realisasi JKN ".date('Ymd-Hi').".xls");
}
?>
<div class="table-responsive">
  <table id="tbl-rekapitulasi" class="table table-bordered" style="font-size: 10pt" border="1">
    <thead class="text-center">
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Nama Belanja</th>
        <th colspan="<?=count($rbelanja)?>">Kategori</th>
      </tr>
      <tr>
        <?php
        foreach($rbelanja as $r) {
          ?>
          <th><?=$r[COL_BELNAMA]?></th>
          <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach ($res as $r) {
        ?>
        <tr>
          <td><?=$no?></td>
          <td><?=$r[COL_BRGNAMA]?></td>
          <?php
          foreach($rbelanja as $b) {
            ?>
            <td class="text-right"><?=$r[COL_IDBELANJA]==$b[COL_UNIQ]?number_format($r[COL_BELTOTAL]):0?></td>
            <?php
          }
          ?>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
  </table>
</div>
<?php
if(empty($cetak)) {
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
    var dt = $('#tbl-rekapitulasi').dataTable({
      "autoWidth" : false,
      "scrollY" : '40vh',
      "fixedColumns": true,
      "fixedHeader": true,
      "scrollX": true,
      "ordering": false,
      "iDisplayLength": 50,
    });
  });
  </script>
  <?php
}
?>
