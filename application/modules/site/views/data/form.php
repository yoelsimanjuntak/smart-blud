<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/data/index')?>">Realisasi Belanja</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-header">
            <a href="<?=site_url('site/data/index')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group row">
                      <label class="control-label col-sm-3">Tanggal</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control datepicker text-right" name="<?=COL_BELTANGGAL?>" value="<?=!empty($data[COL_BELTANGGAL]) ? $data[COL_BELTANGGAL] : ''?>" placeholder="Tanggal" required />
                      </div>
                    </div>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN) {
                      ?>
                      <div class="form-group row">
                        <label class="control-label col-sm-3">Unit</label>
                        <div class="col-sm-8">
                          <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%">
                            <?=GetCombobox("select * from munit order by UnitNama", COL_UNIQ, COL_UNITNAMA, (!empty($data)?$data[COL_IDUNIT]:null))?>
                          </select>
                        </div>
                      </div>
                      <?php
                    }
                    ?>
                    <div class="form-group row">
                      <label class="control-label col-sm-3">No. Referensi</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="<?=COL_BELREFNO?>" value="<?=!empty($data[COL_BELREFNO]) ? $data[COL_BELREFNO] : ''?>" placeholder="No. Pesanan / No. Kontrak / No. Kwitansi" required />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="alert alert-warning">
                  <h6 class="font-weight-bold">PETUNJUK</h6>
                  <p class="mb-0">Harap mengisi form sesuai dengan teliti.</p>
                  <ul class="text-sm mb-0" style="padding-inline-start: 25px !important">
                    <li>Kolom <strong>Tanggal</strong> diisi dengan tanggal yang tertera pada kwitansi / nota pembayaran.</li>
                    <li>Kolom <strong>Kategori Barang</strong> disesuaikan dengan kelompok barang / jasa yang dibelanjakan.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-header">
            <h5 class="card-title font-weight-bold">RINCIAN</h5>
          </div>
          <div class="card-body p-0">
            <table id="item-table" class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap; padding: .75rem !important"><button type="button" class="btn btn-xs btn-outline-success" id="item-add"><i class="far fa-plus-circle"></i></button</th>
                  <th>Kategori</th>
                  <th style="width: 200px; white-space: nowrap;">Volume</th>
                  <th style="width: 200px; white-space: nowrap;">Satuan</th>
                  <th>Keterangan</th>
                  <th style="width: 200px; white-space: nowrap; padding-right: 1.5rem !important" class="text-right">Total</th>
                </tr>
              </thead>
              <tbody></tbody>
              <tfoot>
                <tr>
                  <th class="text-right" colspan="5">GRAND TOTAL</th>
                  <th class="text-right font-weight-bold" style="padding-right: 1.5rem !important"><span id="item-grandtotal"></span></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div id="hidden-input" class="row d-none">
      <?php
      if(isset($det) && !empty($det)) {
        foreach($det as $d) {
          ?>
          <div class="form-group" data-index="<?=$d[COL_UNIQ]?>">
            <input type="hidden" name="Uniq[]" value="<?=$d[COL_UNIQ]?>" />
            <input type="hidden" name="IdBarang[]" value="<?=$d[COL_IDBARANG]?>" />
            <input type="hidden" name="NmBarang[]" value="<?=$d[COL_BRGNAMA]?>" />
            <input type="hidden" name="BelVolume[]" value="<?=$d[COL_BELVOLUME]?>" />
            <input type="hidden" name="BelSatuan[]" value="<?=$d[COL_BELSATUAN]?>" />
            <input type="hidden" name="BelTotal[]" value="<?=$d[COL_BELTOTAL]?>" />
            <input type="hidden" name="BelCatatan[]" value="<?=$d[COL_BELCATATAN]?>" />
          </div>
          <?php
        }
      }
      ?>
    </div>
    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modal-item" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">TAMBAH / UBAH ITEM</span>
      </div>
      <form id="form-item" action="" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" name="item-idbarang" style="width: 100%" required>
              <?=GetCombobox("select * from mbarang where IsDeleted=0 order by BrgNama", COL_UNIQ, COL_BRGNAMA)?>
            </select>
          </div>
          <div class="form-group">
            <label>Volume / Satuan</label>
            <div class="row">
              <div class="col-sm-3">
                <input type="text" class="form-control uang text-right" name="item-volume" placeholder="Volume" required />
              </div>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="item-satuan" placeholder="Satuan" required />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Total</label>
            <div class="row">
              <div class="col-sm-8">
                <input type="text" class="form-control uang text-right" name="item-total" placeholder="Total" required />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" name="item-keterangan" placeholder="Catatan / Keterangan Tambahan"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="button" class="btn btn-sm btn-outline-success" id="item-add-button"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
function drawItems() {
  var rowItem = $('.form-group', $('#hidden-input'));
  var rowGrandTotal = 0;
  var rowHtml = '';

  if(rowItem.length > 0) {
    for (var i=0; i<rowItem.length; i++) {
      var rowIdx = $(rowItem[i]).data('index');
      var rowUniq = $('[name^=Uniq]', $(rowItem[i])).val();
      var rowNm = $('[name^=NmBarang]', $(rowItem[i])).val();
      var rowVol = $('[name^=BelVolume]', $(rowItem[i])).val();
      var rowSatuan = $('[name^=BelSatuan]', $(rowItem[i])).val();
      var rowTotal = $('[name^=BelTotal]', $(rowItem[i])).val();
      var rowKeterangan = $('[name^=BelCatatan]', $(rowItem[i])).val();
      rowHtml += '<tr data-index="'+rowIdx+'">';
      rowHtml += '<td class="text-center" style="width: 10px; white-space: nowrap; padding: .75rem !important"><button type="button" class="btn btn-xs btn-outline-danger item-del-button"><i class="far fa-trash"></i></button></td>';
      rowHtml += '<td>'+rowNm+'</td>';
      rowHtml += '<td class="text-right">'+parseFloat(rowVol).toLocaleString('en-ID')+'</td>';
      rowHtml += '<td>'+rowSatuan+'</td>';
      rowHtml += '<td>'+rowKeterangan+'</td>';
      rowHtml += '<td class="text-right" style="width: 10px; white-space: nowrap; padding-right: 1.5rem !important">'+parseFloat(rowTotal).toLocaleString('en-ID')+'</td>';
      rowHtml += '</tr>';

      rowGrandTotal += parseFloat(rowTotal);
    }
  } else {
    rowHtml = '<tr><td colspan="6" class="text-center">(KOSONG)</td></tr>';
  }

  $('tbody', $('#item-table')).empty();
  $('tbody', $('#item-table')).append(rowHtml);
  $('#item-grandtotal').html(parseFloat(rowGrandTotal).toLocaleString('en-ID'));

  $('.item-del-button', $('#item-table')).click(function(){
    var idx_ = $(this).closest('tr').data('index');

    if(confirm('Apakah anda yakin ingin menghapus item ini?')) {
      $('div[data-index="'+idx_+'"]',$('#hidden-input')).remove();
      drawItems();
    }
  });
}
$(document).ready(function() {
  drawItems();
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          console.log(res);
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                //location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          console.log(data)
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#item-add').click(function(){
    $('#modal-item').modal('show');
  });

  $('#item-add-button').click(function(){
    var itemIdBarang = $('[name=item-idbarang]', $('#modal-item')).val();
    var itemNmBarang = $('option:selected', $('[name=item-idbarang]', $('#modal-item'))).text();
    var itemVol = $('[name=item-volume]', $('#modal-item')).val();
    var itemSatuan = $('[name=item-satuan]', $('#modal-item')).val();
    var itemTotal = $('[name=item-total]', $('#modal-item')).val();
    var itemKeterangan = $('[name=item-keterangan]', $('#modal-item')).val();

    if(!itemIdBarang || !itemVol || !itemSatuan || !itemTotal) {
      alert('Mohon isi data rincian barang dengan lengkap!');
      return false;
    }

    var itemHtml = '';
    itemHtml += '<div class="form-group" data-index="'+moment().format('YYYYMMDDhhmmss')+'">';
    itemHtml += '<input type="hidden" name="IdBarang[]" value="'+itemIdBarang+'" />';
    itemHtml += '<input type="hidden" name="NmBarang[]" value="'+itemNmBarang+'" />';
    itemHtml += '<input type="hidden" name="BelVolume[]" value="'+itemVol+'" />';
    itemHtml += '<input type="hidden" name="BelSatuan[]" value="'+itemSatuan+'" />';
    itemHtml += '<input type="hidden" name="BelTotal[]" value="'+itemTotal+'" />';
    itemHtml += '<input type="hidden" name="BelCatatan[]" value="'+itemKeterangan+'" />';
    itemHtml += '</div>';
    $('#hidden-input').append(itemHtml);
    $('#modal-item').modal('hide');
    $('input,textarea', $('#modal-item')).val('');
    drawItems();
  });
});
</script>
