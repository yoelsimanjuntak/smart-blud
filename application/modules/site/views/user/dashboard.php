<?php
$numPagu = 0;
$numRealisasi = 0;
$ruser = GetLoggedUser();
$runit = $this->db->count_all_results(TBL_MUNIT);
$rdba = $this->db
->where(COL_DBAISAKTIF,1)
->get(TBL_TDBA)
->row_array();
if(!empty($rdba)) {
  $rdba_sum = $this->db
  ->select_sum(COL_PAGU)
  ->where(COL_IDDBA, $rdba[COL_UNIQ])
  ->get(TBL_TDBA_DET)
  ->row_array();
  if(!empty($rdba_sum)) {
    $numPagu = $rdba_sum[COL_PAGU];
  }
}
echo $this->db->last_query();

 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?= strtoupper($title) ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($runit)?></h3>

              <p>Unit Kerja</p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=number_format($numPagu)?></h3>
              <p>Pagu</p>
            </div>
            <div class="icon">
              <i class="far fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>-</h3>
              <p>Realisasi Pembayaran</p>
            </div>
            <div class="icon">
              <i class="far fa-wallet"></i>
            </div>
          </div>
        </div>
        <?php
      } else {
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=number_format($numPagu)?></h3>

              <p>Pagu</p>
            </div>
            <div class="icon">
              <i class="far fa-database"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>-</h3>

              <p>Realisasi</p>
            </div>
            <div class="icon">
              <i class="far fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      }
      ?>

    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
});
</script>
