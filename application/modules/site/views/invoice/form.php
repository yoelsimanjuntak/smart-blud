<?php
$ruser=GetLoggedUser();
?>
<form id="form-invoice" action="<?=current_url()?>" enctype="multipart/form-data">
  <?php
  if($ruser[COL_ROLEID]!=ROLEPPTK) {
    ?>
    <div class="form-group">
      <label>Unit Kerja</label>
      <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%">
        <?=GetCombobox("select * from munit order by UnitNama", COL_UNIQ, COL_UNITNAMA, (!empty($data)?$data[COL_IDUNIT]:null))?>
      </select>
    </div>
    <?php
  }
  ?>
  <div class="form-group">
    <label>Belanja</label>
    <select class="form-control" name="<?=COL_IDDBA?>" style="width: 100%">
      <?=GetCombobox("select det.* from tdba_det det left join tdba on tdba.Uniq = det.IdDba where tdba.DBAIsAktif=1 order by det.DBARekening", COL_UNIQ, array(COL_DBAREKENING, COL_DBAKETERANGAN), (!empty($data)?$data[COL_IDDBA]:null))?>
    </select>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-9">
        <label>Nomor</label>
        <input type="text" class="form-control" name="<?=COL_PEMBNOMOR?>" value="<?=!empty($data)?$data[COL_PEMBNOMOR]:''?>" required />
      </div>
      <div class="col-sm-3">
        <label>Nominal</label>
        <input type="text" class="form-control text-right uang" name="<?=COL_PEMBNOMINAL?>" value="<?=!empty($data)?$data[COL_PEMBNOMINAL]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" rows="4" name="<?=COL_PEMBKETERANGAN?>"><?=!empty($data)?$data[COL_PEMBKETERANGAN]:''?></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  $("select", $('#form-invoice')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-invoice').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
