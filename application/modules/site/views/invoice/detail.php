<?php
$rdoc = $this->db
->where(COL_IDPEMBAYARAN, $data[COL_UNIQ])
->order_by(COL_CREATEDON, 'desc')
->get(TBL_TPEMBAYARAN_DOC)
->result_array();

$rlog = $this->db
->where(COL_IDPEMBAYARAN, $data[COL_UNIQ])
->order_by(COL_CREATEDON, 'desc')
->get(TBL_TPEMBAYARAN_LOG)
->result_array();

$htmlStatus = '<span class="badge badge-secondary">'.$data[COL_PEMBSTATUS].'</span>';
if($data[COL_PEMBSTATUS]=='VERIFIKASI') $htmlStatus = '<span class="badge badge-primary">'.$data[COL_PEMBSTATUS].'</span>';
else if($data[COL_PEMBSTATUS]=='PEMBAYARAN') $htmlStatus = '<span class="badge badge-info">'.$data[COL_PEMBSTATUS].'</span>';
else if($data[COL_PEMBSTATUS]=='SELESAI') $htmlStatus = '<span class="badge badge-success">'.$data[COL_PEMBSTATUS].'</span>';
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$data[COL_PEMBNOMOR]?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/invoice/index')?>">Pembayaran</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h5 class="card-title">Rincian Pembayaran</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tr>
                <td style="width: 200px; white-space: nowrap">Nomor</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=$data[COL_PEMBNOMOR]?></td>
              </tr>
              <tr>
                <td style="width: 200px; white-space: nowrap">Rekening</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=$data[COL_DBAREKENING].' - '.$data[COL_DBAKETERANGAN]?></td>
              </tr>
              <tr>
                <td style="width: 200px; white-space: nowrap">Unit Kerja</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=$data[COL_UNITNAMA]?></td>
              </tr>
              <tr>
                <td style="width: 200px; white-space: nowrap">Nominal (Rp.)</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=!empty($data[COL_PEMBNOMINAL])?number_format($data[COL_PEMBNOMINAL]):0?></td>
              </tr>
              <tr>
                <td style="width: 200px; white-space: nowrap">Status</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=$htmlStatus?></td>
              </tr>
              <tr>
                <td style="width: 200px; white-space: nowrap">Keterangan</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="font-weight-bold"><?=$data[COL_PEMBKETERANGAN]?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-success">
          <div class="card-header">
            <h5 class="card-title"><i class="far fa-file-alt"></i>&nbsp;Lampiran</h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool"><i class="fas fa-plus-circle"></i>&nbsp;UNGGAH</button>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered text-sm">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">WAKTU</th>
                  <th>KETERANGAN</th>
                  <th style="width: 10px; white-space: nowrap">OPSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rdoc)) {
                  foreach($rdoc as $r) {
                    ?>
                    <tr>
                      <td class="text-right"><?=date('d-m-Y H:i', strtotime($r[COL_CREATEDON]))?></td>
                      <td><?=$r[COL_DOCJUDUL]?></td>
                      <td class="text-center">
                        <a href="<?=MY_UPLOADURL.$r[COL_DOCFILE]?>" class="btn btn-outline-success btn-xs" target=""><i class="far fa-download"></i></a>&nbsp;
                        <a href="<?=site_url('site/invoice/doc-delete/'.$d[COL_UNIQ])?>" class="btn btn-outline-danger btn-xs btn-del-doc"><i class="far fa-times-circle"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3" class="font-italic text-center">(KOSONG)</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-info">
          <div class="card-header">
            <h5 class="card-title"><i class="far fa-history"></i>&nbsp;Riwayat</h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool"><i class="fas fa-plus-circle"></i>&nbsp;TAMBAH</button>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered text-sm">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">WAKTU</th>
                  <th>KETERANGAN</th>
                  <th style="width: 10px; white-space: nowrap">STATUS</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rlog)) {
                  foreach($rlog as $r) {

                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3" class="font-italic text-center">(KOSONG)</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
