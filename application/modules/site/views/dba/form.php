<form id="form-dba" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-9">
        <label>Judul</label>
        <input type="text" class="form-control" name="<?=COL_DBAJUDUL?>" value="<?=!empty($data)?$data[COL_DBAJUDUL]:''?>" required />
      </div>
      <div class="col-sm-3">
        <label>Tahun</label>
        <input type="text" class="form-control text-right" name="<?=COL_DBATAHUN?>" value="<?=!empty($data)?$data[COL_DBATAHUN]:date('Y')?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" rows="4" name="<?=COL_DBAKETERANGAN?>"><?=!empty($data)?$data[COL_DBAKETERANGAN]:''?></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  $("select", $('#form-dba')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-dba').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
