<form id="form-dba" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-7">
        <label>Kode Rekening</label>
        <input type="text" class="form-control" name="<?=COL_DBAREKENING?>" value="<?=!empty($data)?$data[COL_DBAREKENING]:''?>" required />
      </div>
      <div class="col-sm-5">
        <label>Pagu</label>
        <input type="text" class="form-control text-right uang" name="<?=COL_PAGU?>" value="<?=!empty($data)?$data[COL_PAGU]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Uraian</label>
    <textarea class="form-control" rows="4" name="<?=COL_DBAKETERANGAN?>"><?=!empty($data)?$data[COL_DBAKETERANGAN]:''?></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  $("select", $('#form-dba')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $(".uang", $('#form-dba')).number(true, 0, '.', ',');
  $('#form-dba').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            location.reload();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
