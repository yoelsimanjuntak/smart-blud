<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$data[COL_DBAJUDUL]?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('site/dba/index')?>">Anggaran</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<?php
$rbelanja1 = $this->db
->order_by(COL_BELREKENING, 'asc')
->get(TBL_MBELANJA1)
->result_array();
$sumTotal = 0;
?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th class="text-center" style="width: 10px; white-space: nowrap">KODE REKENING</th>
                  <th class="text-center">URAIAN</th>
                  <th class="text-center" style="width: 200px; white-space: nowrap">PAGU</th>
                  <th class="text-center" style="width: 10px; white-space: nowrap">OPSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($rbelanja1 as $rb1) {
                  $rbelanja2 = $this->db
                  ->where(COL_IDBELANJA1, $rb1[COL_UNIQ])
                  ->order_by(COL_BELREKENING, 'asc')
                  ->get(TBL_MBELANJA2)
                  ->result_array();
                  $rbelanja2_sum = $this->db
                  ->select_sum(COL_PAGU)
                  ->join(TBL_TDBA_DET,TBL_TDBA_DET.'.'.COL_IDBELANJA." = ".TBL_MBELANJA2.".".COL_UNIQ,"inner")
                  ->where(COL_IDBELANJA1, $rb1[COL_UNIQ])
                  ->get(TBL_MBELANJA2)
                  ->row_array();
                  ?>
                  <tr>
                    <td class="font-weight-bold"><?=$rb1[COL_BELREKENING]?></td>
                    <td class="font-weight-bold"><?=$rb1[COL_BELNAMA]?></td>
                    <td class="font-weight-bold text-right"><?=!empty($rbelanja2_sum)?number_format($rbelanja2_sum[COL_PAGU]):0?></td>
                    <td></td>
                  </tr>
                  <?php
                  foreach($rbelanja2 as $rb2) {
                    $rdet = $this->db
                    ->where(COL_IDDBA, $data[COL_UNIQ])
                    ->where(COL_IDBELANJA, $rb2[COL_UNIQ])
                    ->order_by(COL_DBAREKENING, 'asc')
                    ->get(TBL_TDBA_DET)
                    ->result_array();
                    $rdet_sum = $this->db
                    ->select_sum(COL_PAGU)
                    ->where(COL_IDDBA, $data[COL_UNIQ])
                    ->where(COL_IDBELANJA, $rb2[COL_UNIQ])
                    ->get(TBL_TDBA_DET)
                    ->row_array();
                    ?>
                    <tr>
                      <td class="font-weight-bold"><?=$rb2[COL_BELREKENING]?></td>
                      <td class="font-weight-bold pl-4"><?=$rb2[COL_BELNAMA]?></td>
                      <td class="font-weight-bold text-right"><?=!empty($rdet_sum)?number_format($rdet_sum[COL_PAGU]):0?></td>
                      <td style="width: 10px; white-space: nowrap"><a href="<?=site_url('site/dba/detail-add/'.$rb2[COL_UNIQ].'/'.$data[COL_UNIQ])?>" class="btn btn-block btn-outline-primary btn-xs btn-add-det"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</a></td>
                    </tr>
                    <?php
                    foreach($rdet as $d) {
                      ?>
                      <tr>
                        <td class="font-italic"><?=$d[COL_DBAREKENING]?></td>
                        <td class="font-italic pl-5"><?=$d[COL_DBAKETERANGAN]?></td>
                        <td class="text-right"><?=number_format($d[COL_PAGU])?></td>
                        <td style="width: 10px; white-space: nowrap">
                          <a href="<?=site_url('site/dba/detail-edit/'.$d[COL_UNIQ])?>" class="btn btn-outline-success btn-xs btn-add-det"><i class="far fa-edit"></i>&nbsp;UBAH</a>&nbsp;
                          <a href="<?=site_url('site/dba/detail-delete/'.$d[COL_UNIQ])?>" class="btn btn-outline-danger btn-xs btn-del-det"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
                        </td>
                      </tr>
                      <?php
                      $sumTotal += $d[COL_PAGU];
                    }
                  }
                }
                ?>
                <tr>
                  <td colspan="2" class="font-weight-bold text-center">TOTAL</td>
                  <td class="font-weight-bold text-right"><?=number_format($sumTotal)?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">RINCIAN BELANJA</span>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modal = $('#modal-form');
$(document).ready(function() {
  $('.btn-add-det').click(function(){
    var url = $(this).attr('href');
    $('.modal-body', modal).html('<p class="text-center font-italic">MEMUAT...</p>');
    modal.modal('show');

    $('.modal-body', modal).load(url, function(){
      $('button[type=submit]').unbind('click').click(function(){
        $('form', modal).submit();
      });
    });
    return false;
  });

  $('.btn-del-det').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin ingin menghapus rincian ini?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        location.reload();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });
});
</script>
