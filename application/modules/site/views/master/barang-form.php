<form id="form-doc" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <label>Kategori Belanja</label>
    <select class="form-control" name="<?=COL_IDBELANJA?>" style="width: 100%">
      <?=GetCombobox("select * from mbelanja where IsDeleted=0 order by BelNama", COL_UNIQ, COL_BELNAMA, (!empty($data)?$data[COL_IDBELANJA]:null))?>
    </select>
  </div>
  <div class="form-group">
    <label>Kategori Barang</label>
    <input type="text" class="form-control" name="<?=COL_BRGNAMA?>" value="<?=!empty($data)?$data[COL_BRGNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" rows="4" name="<?=COL_BRGKETERANGAN?>"><?=!empty($data)?$data[COL_BRGKETERANGAN]:''?></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $("select", $('#form-doc')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-doc').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
