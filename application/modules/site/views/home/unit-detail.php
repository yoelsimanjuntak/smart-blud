<?php
$berita = $this->mpost->search(9,"",1,$data[COL_UNIQ]);
$infografis = $this->mpost->search(10,"",2,$data[COL_UNIQ]);
$docs = $this->mpost->search(5,"",3,$data[COL_UNIQ]);
?>
<section class="news-single section" style="background: #f9f9f9 !important; padding-top: 30px !important">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="single-main">
          <!-- News Title -->
          <h1 class="news-title"><?=($data[COL_UNITTIPE]=='PUSKESMAS'?'PUSKESMAS ':'').strtoupper($data[COL_UNITNAMA])?></h1>
          <!-- Meta -->
          <!--<div class="meta">
            <div class="meta-left">
              <span class="author"><i class="far fa-user"></i>&nbsp;--</span>
              <span class="author"><i class="far fa-clock"></i>&nbsp;--</span>
            </div>
            <div class="meta-right" style="margin-top: 0 !important">
              <span class="views"><i class="fa fa-eye"></i>--</span>
            </div>
          </div>-->
          <!-- News Text -->
          <div class="news-text">
            <table class="table">
              <tr>
                <td style="max-width: 75px !important">Alamat</td>
                <td style="width: 10px !important">:</td>
                <td><?=$data[COL_UNITALAMAT]?></td>
              </tr>
              <tr>
                <td style="max-width: 75px !important">Telepon / Kontak</td>
                <td style="width: 10px !important">:</td>
                <td><?=!empty($data[COL_UNITTELP])?$data[COL_UNITTELP]:'-'?></td>
              </tr>
              <tr>
                <td style="max-width: 75px !important">Email</td>
                <td style="width: 10px !important">:</td>
                <td><?=!empty($data[COL_UNITEMAIL])?$data[COL_UNITEMAIL]:'-'?></td>
              </tr>
              <tr>
                <td style="max-width: 75px !important">Kepala <?=($data[COL_UNITTIPE]=='PUSKESMAS'?'Puskesmas ':'')?></td>
                <td style="width: 10px !important">:</td>
                <td>
                  <?=$data[COL_UNITPIMPINAN]?><br />
                  <small><?=$data[COL_UNITPIMPINANPANGKAT]?></small><br />
                  <small>NIP. <?=$data[COL_UNITPIMPINANNIP]?></small>
                </td>
              </tr>
              <?php
              if(!empty($data[COL_UNITGOOGLELINK])) {
                ?>
                <tr>
                  <td colspan="3"><?=$data[COL_UNITGOOGLELINK]?></td>
                </tr>
                <?php
              }
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
if(!empty($berita)) {
  ?>
  <section class="blog section" id="blog" style="padding-top: 0 !important; background: #f9f9f9 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title" style="margin-bottom: 30px !important">
            <h4>BERITA TERKINI</h4>
            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-img.png" style="width: 200px">
          </div>
        </div>
      </div>
      <div class="row">
        <?php
        $n=0;
        foreach($berita as $b) {
          $n++;
          $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
          $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
          ?>
          <div class="col-lg-4 col-md-6 col-12" style="padding-top: 20px!important">
            <div class="single-news">
              <div class="news-head">
                <div style="
                height: 250px;
                width: 100%;
                background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                ">
                </div>
              </div>
              <div class="news-body">
                <div class="news-content">
                  <div class="date"><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></div>
                  <h2>
                    <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h5 class="text-red"><?=$b[COL_POSTTITLE]?></h5></a>
                  </h2>
                  <p class="text" style="text-transform: none !important">
                    <?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </section>
  <?php
}
?>
