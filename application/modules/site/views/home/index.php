<?php
$rhome1 = $this->db->where(COL_CONTENTID, 'TxtWelcome1')->get(TBL__HOMEPAGE)->row_array();
$rhome2 = $this->db->where(COL_CONTENTID, 'TxtWelcome2')->get(TBL__HOMEPAGE)->row_array();
$rprofile1 = $this->db->where(COL_CONTENTID, 'TxtPopup1')->get(TBL__HOMEPAGE)->result_array();
$rprofile2 = $this->db->where(COL_CONTENTID, 'TxtPopup2')->get(TBL__HOMEPAGE)->result_array();
$rprofile3 = $this->db->where(COL_CONTENTID, 'TxtPopup3')->get(TBL__HOMEPAGE)->result_array();
$rslider = $this->db->where(COL_CONTENTID, 'Carousel')->get(TBL__HOMEPAGE)->result_array();
$rnum = $this->db->where(COL_CONTENTID, 'NumProfile')->get(TBL__HOMEPAGE)->result_array();
$rcarousel = $this->db->where(COL_CONTENTID, 'Carousel')->where('ContentDesc2 is not null')->get(TBL__HOMEPAGE)->result_array();
?>
<section class="slider">
  <div class="hero-slider">
    <?php
    if(!empty($rcarousel)) {
      foreach($rcarousel as $car) {
        ?>
        <div class="single-slider" style="background-image:url('<?=MY_UPLOADURL.$car[COL_CONTENTDESC2]?>')">
          <div class="container">
            <div class="row">
              <div class="col-lg-7">
                <div class="text">
                  <h1>SELAMAT <span>DATANG</span></h1>
                  <p style="font-size: 18pt !important; line-height: 1.5 !important">di Website Resmi <br /><strong>DINAS <span style="color: #3D8361 !important">KESEHATAN</span> <?=GetSetting('SETTING_ORG_REGION')?></strong></p>
                  <div class="button">
                    <!--<a href="#" class="btn">Profil</a>-->
                    <a href="#contact-us" class="btn primary">Hubungi Kami</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
    } else {
      ?>
      <div class="single-slider" style="background-image:url('<?=base_url()?>assets/themes/mediplus-lite/img/slider.png')">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <div class="text">
                <h1>SELAMAT <span>DATANG</span></h1>
                <p style="font-size: 18pt !important; line-height: 1.5 !important">di Website Resmi <br /><strong>DINAS <span style="color: #3D8361 !important">KESEHATAN</span> <?=GetSetting('SETTING_ORG_REGION')?></strong></p>
                <div class="button">
                  <!--<a href="#" class="btn">Profil</a>-->
                  <a href="#contact-us" class="btn primary">Hubungi Kami</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="single-slider" style="background-image:url('<?=base_url()?>assets/themes/mediplus-lite/img/slider2.png')">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <div class="text">
                <h1>SELAMAT <span>DATANG</span></h1>
                <p style="font-size: 18pt !important; line-height: 1.5 !important">di Website Resmi <br /><strong>DINAS <span style="color: #3D8361 !important">KESEHATAN</span> <?=GetSetting('SETTING_ORG_REGION')?></strong></p>
                <div class="button">
                  <!--<a href="#" class="btn">Profil</a>-->
                  <a href="#contact-us" class="btn primary">Hubungi Kami</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</section>
<section class="schedule">
  <div class="container">
    <div class="schedule-inner">
      <div class="row" style="display: flex !important; align-items: stretch !important">
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-building"></i>
              </div>
              <div class="single-content">
                <!--<span>Lorem Amet</span>-->
                <h4>PROFIL</h4>
                <ul class="time-sidual">
                  <?php
                  foreach($rprofile1 as $r) {
                    ?>
                    <li class="day"><a href="" data-toggle="modal" data-target="#modal-profile-<?=$r[COL_UNIQ]?>" style="margin-top: 0 !important"><?=$r[COL_CONTENTTITLE]?></a></li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-map-signs"></i>
              </div>
              <div class="single-content">
                <!--<span>Lorem Amet</span>-->
                <h4>BIDANG</h4>
                <ul class="time-sidual">
                  <?php
                  foreach($rprofile2 as $r) {
                    ?>
                    <li class="day"><a href="" data-toggle="modal" data-target="#modal-profile-<?=$r[COL_UNIQ]?>" style="margin-top: 0 !important"><?=$r[COL_CONTENTTITLE]?></a></li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-globe"></i>
              </div>
              <div class="single-content">
                <!--<span>Donec luctus</span>-->
                <h4>UPTD</h4>
                <ul class="time-sidual">
                  <?php
                  foreach($rprofile3 as $r) {
                    ?>
                    <li class="day"><a href="" data-toggle="modal" data-target="#modal-profile-<?=$r[COL_UNIQ]?>" style="margin-top: 0 !important"><?=$r[COL_CONTENTTITLE]?></a></li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="why-choose section" style="padding-top: 0 !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-title">
          <h3><?=strtoupper($this->setting_org_name)?></h3>
          <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-img.png" alt="#">
        </div>
      </div>
    </div>
    <div class="row" style="margin-bottom: 50px !important">
      <div class="col-lg-6 col-12">
        <div class="choose-left">
          <h3>Kata Sambutan</h3>
          <?php
          if(!empty($rhome1)) {
            if(!empty($rhome1[COL_CONTENTDESC2])) {
              ?>
              <p class="text-center">
                <img src="<?=MY_UPLOADURL.$rhome1[COL_CONTENTDESC2]?>" style="width: 200px; height: auto" />
              </p>
              <?php
            }
            ?>
            <p class="text-center"><?=$rhome1[COL_CONTENTDESC1]?></p>
            <?php
          }
          ?>
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <div class="choose-left">
          <h3>Struktur Organisasi</h3>
          <p class="text-center"><?=$rhome2[COL_CONTENTDESC1]?></p>
          <?php
          if(!empty($rhome2)) {
            if(!empty($rhome2[COL_CONTENTDESC2])) {
              ?>
              <p class="text-center">
                <a href="#" data-toggle="modal" data-target="#modal-struktur"><img src="<?=MY_UPLOADURL.$rhome2[COL_CONTENTDESC2]?>" style="width: 100%; height: auto" /></a>
              </p>
              <?php
            }
            ?>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="fun-facts" class="fun-facts section overlay" style="background:url(<?=base_url()?>assets/themes/mediplus-lite/img/fun-bg.jpg) !important">
  <div class="container">
    <div class="row">
      <?php
      foreach($rnum as $r) {
        ?>
        <div class="col-lg-3 col-md-6 col-12">
          <div class="single-fun">
            <i class="far fa-hashtag"></i>
            <div class="content">
              <span class="counter"><?=$r[COL_CONTENTDESC3]?></span>
              <p><?=$r[COL_CONTENTDESC1]?></p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>

<?php
if(!empty($berita)) {
  ?>
  <section class="blog section" id="blog" style="background: #f9f9f9 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h2>BERITA TERKINI</h2>
            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-img.png">
          </div>
        </div>
      </div>
      <div class="row">
        <?php
        $n=0;
        foreach($berita as $b) {
          $n++;
          $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
          $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
          ?>
          <div class="col-lg-4 col-md-6 col-12" style="padding-top: 20px!important">
            <div class="single-news">
              <div class="news-head">
                <div style="
                height: 250px;
                width: 100%;
                background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                ">
                </div>
              </div>
              <div class="news-body">
                <div class="news-content">
                  <div class="date"><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></div>
                  <h2>
                    <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h5 class="text-red"><?=$b[COL_POSTTITLE]?></h5></a>
                  </h2>
                  <p class="text" style="text-transform: none !important">
                    <?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </section>
  <?php
}
?>

<?php
if(!empty($galeri)) {
  ?>
  <section class="portfolio section" style="background: #FFF !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h2>INFOGRAFIS</h2>
            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-img.png">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-12">
          <div id="owl-pegawai" class="owl-carousel galeri-slider">
            <?php
            foreach($galeri as $r) {
              $rfiles = $this->db
              ->where(COL_POSTID, $r[COL_POSTID])
              ->get(TBL__POSTIMAGES)
              ->result_array();
              foreach($rfiles as $f) {
                ?>
                <div class="single-pf" style="width: 300px; height: 300px; background-image: url(<?=MY_UPLOADURL.$f[COL_IMGPATH]?>); background-size: cover">
                  <a href="<?=site_url('site/ajax/popup-galeri/'.$f[COL_POSTIMAGEID])?>" class="btn btn-popup-galeri">LIHAT</a>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
}
?>

<section class="appointment" id="contact-us" style="background: #f9f9f9 !important; padding-top: 170px !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-title">
          <h2>HUBUNGI KAMI</h2>
          <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-img.png">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12 col-12">
        <form class="form" action="#">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
              <div class="form-group">
                <input name="name" type="text" placeholder="Nama Lengkap">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="form-group">
                <input name="email" type="email" placeholder="Email">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="form-group">
                <input name="phone" type="text" placeholder="No. Telp / HP">
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
              <div class="form-group">
                <textarea name="message" placeholder="Tuliskan pesan / catatan anda disini.."></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-4 col-12">
              <div class="form-group">
                <div class="button">
                  <button type="submit" class="btn">SUBMIT</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-lg-6 col-md-12 ">
        <div class="appointment-image">
          <img src="<?=MY_IMAGEURL.'img-contact-us.png'?>" alt="#">
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-galeri" aria-modal="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">GALERI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <p class="p-2">MEMUAT...</p>
      </div>
    </div>
  </div>
</div>
<?php
if(!empty($rhome2)) {
  if(!empty($rhome2[COL_CONTENTDESC2])) {
    ?>
    <div id="modal-struktur" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg" style="max-width: 80vw !important">
        <div class="modal-content" style="width: 80vw !important">
          <div class="modal-header">
            <h4 class="modal-title">Struktur Organisasi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <img src="<?=MY_UPLOADURL.$rhome2[COL_CONTENTDESC2]?>" style="width: 100%; height: auto" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn primary" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
  <?php
}

foreach($rprofile1 as $r) {
  ?>
  <div id="modal-profile-<?=$r[COL_UNIQ]?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?=$r[COL_CONTENTTITLE]?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <?php
          if($r[COL_CONTENTTYPE]=='IMG') {
            ?>
            <img src="<?=MY_UPLOADURL.$r[COL_CONTENTDESC2]?>" style="width: 100%; height: auto" />
            <?php
          } else if($r[COL_CONTENTTYPE]=='PDF') {
            ?>
            <embed src="<?=MY_UPLOADURL.$r[COL_CONTENTDESC2]?>" width="100%" height="480px" type="application/pdf" />
            <?php
          }
          ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn primary" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
        </div>
      </div>
    </div>
  </div>
  <?php
}

foreach($rprofile2 as $r) {
  ?>
  <div id="modal-profile-<?=$r[COL_UNIQ]?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?=$r[COL_CONTENTTITLE]?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <?=$r[COL_CONTENTDESC1]?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn primary" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
        </div>
      </div>
    </div>
  </div>
  <?php
}

foreach($rprofile3 as $r) {
  ?>
  <div id="modal-profile-<?=$r[COL_UNIQ]?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?=$r[COL_CONTENTTITLE]?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body p-0">
          <table class="table table-striped mb-0" style="width: 100%">
            <thead>
              <tr>
                <th>NO.</th>
                <th>NAMA</th>
                <th class="text-center">#</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $res = $this->db->query($r[COL_CONTENTDESC1])->result_array();
              foreach($res as $u) {
                ?>
                <tr>
                  <td style="width: 10px; white-space: nowrap; text-align: right;vertical-align: middle"><?=$no?>.</td>
                  <td style="vertical-align: middle"><strong><?=$u[$r[COL_CONTENTDESC2]]?></strong></td>
                  <td style="width: 10px; white-space: nowrap; color: #fff !important"><a class="btn primary" href="<?=site_url('site/home/unit/'.$u[$r[COL_CONTENTDESC3]])?>">Lihat Profil <i class="far fa-arrow-right"></i></a></td>
                </tr>
                <?php
                $no++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>
<script type="text/javascript">
$(document).ready(function() {
  $("#modal-galeri").on('hidden.bs.modal', function(){
    $('.modal-title', $(this)).html('GALERI');
    $('.modal-body', $(this)).html('<p class="p-2">MEMUAT...</p>');
  });
  var owlGaleri = $('.galeri-slider');
  owlGaleri.owlCarousel({
    autoplay:true,
    autoplayTimeout:4000,
    margin:15,
    smartSpeed:300,
    autoplayHoverPause:true,
    loop:true,
    nav:true,
    dots:false,
    responsive:{
      300: {
        items:1,
      },
      480: {
        items:2,
      },
      768: {
        items:2,
      },
      1170: {
        items:4,
      },
    },
    onInitialized: function(){
      $('.btn-popup-galeri', owlGaleri).click(function(){
        var mod = $('#modal-galeri').modal('show');
        var href = $(this).attr('href');

        $.ajax({
          url: href
        }).done(function(data) {
          var dat = JSON.parse(data);
          $('.modal-body', mod).html(dat.body);
          $('.modal-title', mod).html(dat.title);
        });
        return false;
      });
    }
  });
});
</script>
