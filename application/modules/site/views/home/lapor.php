<div class="content-wrapper mt-0">
  <div class="hero-container" style="padding: 60px 20px !important">
    <span class="hero-wave"></span>
    <div class="hero-content">
      <div class="row">
        <div class="col-sm-12">
          <div class="hero-item text-center" style="transform: translateY(20px) !important">
            <h4 class="font-weight-bold text-white">
              <?=strtoupper($title)?>
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-danger">
            <div class="card-body">
              <form id="form-lapor" method="post" action="<?=current_url()?>" class="form-horizontal">
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>NAMA</label>
                      <input type="text" name="<?=COL_NMPELAPOR?>" class="form-control" required />
                    </div>
                    <div class="col-sm-6">
                      <label>NIK</label>
                      <input type="text" name="<?=COL_NMNIK?>" class="form-control" required />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>ALAMAT</label>
                      <input type="text" name="<?=COL_NMALAMAT?>" class="form-control" required />
                    </div>
                    <div class="col-sm-6">
                      <label>NO. HP</label>
                      <input type="text"  name="<?=COL_NMNOMORHP?>" class="form-control" required />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label>LAPORAN</label>
                      <textarea rows="5"  name="<?=COL_NMLAPORAN?>" class="form-control" required></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group text-center mb-0">
                  <button type="submit" class="btn btn-default">KIRIM LAPORAN</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-light bg-white').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
$(document).ready(function() {
  $('#form-lapor').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 2000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
