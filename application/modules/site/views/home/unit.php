<?php
$arrData = array();
foreach ($data as $r) {
  $arrData[] = array(
    $r[COL_UNITNAMA],
    '<a class="btn primary" href="'.site_url('site/home/unit/'.$r[COL_UNIQ]).'">Lihat Profil <i class="far fa-arrow-right"></i></a>'
  );
}
?>
<div class="breadcrumbs overlay" style="padding: 75px 0px !important; background:url(<?=base_url()?>assets/themes/mediplus-lite/img/client-bg.jpg) !important">
  <div class="container">
    <div class="bread-inner">
      <div class="row">
        <div class="col-12">
          <h2><?=strtoupper($title)?></h2>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="blog section" id="blog" style="background: #f9f9f9 !important; padding-top: 0 !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12">
        <div class="main-sidebar">
          <div class="single-widget search">
            <div class="row">
              <div class="col-sm-12 col-12">
                <table id="datalist" class="table table-bordered"></table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth":false,
    //"bJQueryUI": true,
    "aaData": <?=json_encode($arrData)?>,
    //"scrollY" : '40vh',
    //"scrollX": "120%",
    "iDisplayLength": 50,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-12'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12'p>>",
    "ordering": false,
    "columnDefs": [
      {"targets":[0,1], "className":'nowrap'}
    ],
    "aoColumns": [
        {"sTitle": "NAMA","bSortable":false},
        {"sTitle": "LINK","bSortable":false,"sWidth":"10px"},
    ]
  });
});
</script>
